package app

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq" // For Postgres Conn
	"github.com/spf13/viper"
)

// App : Struct of App package
type App struct {
	Configs    *ConfigStruct
	PostgreDB  *sql.DB
	ConnString string
}

// Appl : Global variable of App
var Appl App

var ctx = context.Background()

// Initialize : Function for initializing App
func Initialize() {
	InitConfig()
	fmt.Println("....Starting " + Appl.Configs.Main.DisplayName + "....")
	ConnPostgres()
}

// InitConfig : Function for initializing App Config
func InitConfig() {
	viper.AddConfigPath("./")
	viper.SetConfigName("go-db-api")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	var conf Config
	err := viper.Unmarshal(&conf)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	if conf.Active == "dev" {
		Appl.Configs = &conf.Dev
	} else {
		Appl.Configs = &conf.Prod
	}
}

// ConnPostgres : Function for connecting to PostgreSQL Server
func ConnPostgres() {
	var err error

	fmt.Printf("[ Connecting To PostgreSQL Server ] : ")

	confPostgres := Appl.Configs.Postgres

	Appl.ConnString = fmt.Sprintf("user=%s password=%s host=%s port=%d dbname=%s sslmode=disable", confPostgres.User, confPostgres.Pass, confPostgres.Host, confPostgres.Port, confPostgres.Db)

	Appl.PostgreDB, err = sql.Open("postgres", Appl.ConnString)
	if err != nil {
		fmt.Println("Failed, unable to open connection to PostgreSQL Server, application terminated...")
		os.Exit(3)
	}

	Appl.ConnString = fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?sslmode=disable", confPostgres.User, confPostgres.Pass, confPostgres.Host, confPostgres.Port, confPostgres.Db)

	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()
	if err = Appl.PostgreDB.PingContext(ctx); err != nil {
		Appl.PostgreDB.Close()
		fmt.Println("Failed, unable to dial PostgreSQL Server, application terminated...", err.Error())
		os.Exit(3)
	}

	fmt.Println("Success")
}
