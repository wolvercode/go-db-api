package app

// Config : Struct of App Config
type Config struct {
	Main   ConfigStruct
	Active string
	Dev    ConfigStruct
	Prod   ConfigStruct
}

// ConfigStruct : Struct of App Config's Item
type ConfigStruct struct {
	Main     MainStruct
	Postgres PostgresStruct
}

// MainStruct : Struct of App Main Config
type MainStruct struct {
	DisplayName string
	Port        int
	Sleep       int
}

//PostgresStruct : Struct of Postgresql Connection
type PostgresStruct struct {
	Host string
	Port int
	User string
	Pass string
	Db   string
}
