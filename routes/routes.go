package routes

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/billing/go-db-api/middleware"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

// Router : Gorilla Mux Router
var Router *mux.Router

// InitializeRoutes : Function for initializing Router
func InitializeRoutes(port int) {
	Router = mux.NewRouter()

	apiRoutes := api{}
	apiRoutes.getRouter()

	negroware := negroni.Classic()
	negroware.Use(negroni.HandlerFunc(middleware.LoggingMiddleware))
	negroware.UseHandler(Router)

	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = 1000

	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(port), handlers.CORS()(negroware)))
}
