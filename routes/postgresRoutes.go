package routes

import (
	"bitbucket.org/billing/go-db-api/handler/handlerpostgres"
	"bitbucket.org/billing/go-db-api/middleware"
	"github.com/gorilla/mux"
)

// PostgreSQLRoutes :
type PostgreSQLRoutes struct {
	mainRoute *mux.Router
}

func (a *PostgreSQLRoutes) getRoutes() {
	a.mainRoute = Router.PathPrefix("/postgresql/process").Subrouter()
	a.processRoutes()
	a.collectionRoutes()
	a.lookupRoutes()
	a.checkdupRoutes()
	a.reconRoutes()
	a.sendFileRoutes()
	a.insertDBRoutes()
	a.regenerateRoutes()
	a.configurationRoutes()
	a.tapCodeRoutes()
	a.dataOnlyRoutes()
	a.alertRoutes()
	a.reportRoutes()
	a.reconTapinOnlyRoutes()
	a.generateL3Routes()
}

// processRoutes : Routes of Common Process
func (a *PostgreSQLRoutes) processRoutes() {
	handlerProcess := handlerpostgres.ProcessHandler{}
	a.mainRoute.Use(middleware.CustomMiddleware)
	a.mainRoute.HandleFunc("/updateStatus/{processID}/{processStatusID}", handlerProcess.UpdateStatusProcess).Methods("PUT")
	a.mainRoute.HandleFunc("/getSlicedProcess/{minStartcall}/{maxStartcall}/{dataTypeID}/{moMt}/{postPre}", handlerProcess.GetSlicedProcess).Methods("GET")
}

// collectionRoutes : Routes of Collection
func (a *PostgreSQLRoutes) collectionRoutes() {
	handlerCollection := handlerpostgres.CollectionHandler{}
	collectionRoute := a.mainRoute.PathPrefix("/collection").Subrouter()
	collectionRoute.HandleFunc("/getSSHost", handlerCollection.GetSSHost).Methods("GET")
	collectionRoute.HandleFunc("/getSSUser", handlerCollection.GetSSUser).Methods("GET")
	collectionRoute.HandleFunc("/getSSPass", handlerCollection.GetSSPass).Methods("GET")
	collectionRoute.HandleFunc("/getSSHome", handlerCollection.GetSSHome).Methods("GET")
	collectionRoute.HandleFunc("/getPathByFiletype/{filetypeID}", handlerCollection.GetPathByFiletype).Methods("GET")
	collectionRoute.HandleFunc("/getSSTarget/{filetypeName}/{datatypeID}", handlerCollection.GetSSTarget).Methods("GET")
	collectionRoute.HandleFunc("/closeProcess", handlerCollection.CloseProcessCollection).Methods("POST")

}

// lookupRoutes : Routes of Lookup
func (a *PostgreSQLRoutes) lookupRoutes() {
	currentHandler := handlerpostgres.LookupHandler{}
	currentRoute := a.mainRoute.PathPrefix("/lookup").Subrouter()
	currentRoute.HandleFunc("/getProcessSplit", currentHandler.GetProcessSplit).Methods("GET")
	currentRoute.HandleFunc("/closeProcessSplit", currentHandler.CloseProcessSplit).Methods("POST")
	currentRoute.HandleFunc("/getProcess", currentHandler.GetProcessLookup).Methods("GET")
	currentRoute.HandleFunc("/getListPartnerID", currentHandler.GetListPartnerID).Methods("GET")
	currentRoute.HandleFunc("/getLastLookup", currentHandler.GetLastLookup).Methods("GET")
	currentRoute.HandleFunc("/updateLookup/{currentLookup}", currentHandler.UpdateLookup).Methods("PUT")
	currentRoute.HandleFunc("/closeProcess", currentHandler.CloseProcessLookup).Methods("POST")
}

// checkdupRoutes : Routes of Check Duplicate
func (a *PostgreSQLRoutes) checkdupRoutes() {
	handlerCheckdup := handlerpostgres.CheckdupHandler{}
	checkdupRoute := a.mainRoute.PathPrefix("/checkdup").Subrouter()
	checkdupRoute.HandleFunc("/getProcess", handlerCheckdup.GetProcessCheckDup).Methods("GET")
	checkdupRoute.HandleFunc("/closeProcess", handlerCheckdup.CloseProcessCheckdup).Methods("POST")
	checkdupRoute.HandleFunc("/getCheckDupConf", handlerCheckdup.GetCheckDupConf).Methods("GET")
}

// reconRoutes : Routes of Recon
func (a *PostgreSQLRoutes) reconRoutes() {
	handlerRecon := handlerpostgres.ReconHandler{}
	reconRoute := a.mainRoute.PathPrefix("/recon").Subrouter()
	reconRoute.HandleFunc("/getProcess", handlerRecon.GetProcessRecon).Methods("GET")
	reconRoute.HandleFunc("/getLevelVolcomp", handlerRecon.GetLevelVolcomp).Methods("GET")
	reconRoute.HandleFunc("/closeProcess", handlerRecon.CloseProcessRecon).Methods("POST")
}

// sendFileRoutes : Routes of Send File
func (a *PostgreSQLRoutes) sendFileRoutes() {
	handlerSendFile := handlerpostgres.SendFileHandler{}
	sendfileRoute := a.mainRoute.PathPrefix("/sendfile").Subrouter()
	sendfileRoute.HandleFunc("/getProcess", handlerSendFile.GetProcessSendFile).Methods("GET")
	sendfileRoute.HandleFunc("/getReceiver", handlerSendFile.GetReceiverSendFile).Methods("GET")
	sendfileRoute.HandleFunc("/closeProcess", handlerSendFile.CloseProcessSendFile).Methods("POST")
}

// insertDBRoutes : Routes of Insert DB
func (a *PostgreSQLRoutes) insertDBRoutes() {
	handlerInsertDB := handlerpostgres.InsertDBHandler{}
	insertDBRoute := a.mainRoute.PathPrefix("/insertdb").Subrouter()
	insertDBRoute.HandleFunc("/getProcess", handlerInsertDB.GetProcessInsertDB).Methods("GET")
	insertDBRoute.HandleFunc("/createTable/{periode}/{dataType}/{fileType}/{postPre}", handlerInsertDB.CreateTableIfNotExists).Methods("GET")
	insertDBRoute.HandleFunc("/ocs/{periode}", handlerInsertDB.InsertOCSToDB).Methods("POST")
	insertDBRoute.HandleFunc("/closeProcess", handlerInsertDB.CloseProcessInsertDB).Methods("POST")
	insertDBRoute.HandleFunc("/tapin/{periode}", handlerInsertDB.InsertTAPINToDB).Methods("POST")
	insertDBRoute.HandleFunc("/tapinonly/{periode}", handlerInsertDB.InsertTAPINOnlyToDB).Methods("POST")
	insertDBRoute.HandleFunc("/copy/ocs/{postPre}/{fileType}", handlerInsertDB.CopyOCSToDB).Methods("POST")
	insertDBRoute.HandleFunc("/copy/tapin/{postPre}/{fileType}", handlerInsertDB.CopyTapinToDB).Methods("POST")
	insertDBRoute.HandleFunc("/copy/tapinonly/{postPre}/{fileType}", handlerInsertDB.CopyTapinOnlyToDB).Methods("POST")
	insertDBRoute.HandleFunc("/copy/match/{postPre}/{fileType}", handlerInsertDB.CopyMatchToDB).Methods("POST")
	insertDBRoute.HandleFunc("/getTask", handlerInsertDB.GetTaskInsertDB).Methods("GET")
	insertDBRoute.HandleFunc("/updateStatus/{processID}/{processStatusID}", handlerInsertDB.UpdateStatusProcess).Methods("POST")
}

// regenerateRoutes : Routes of Regenerate
func (a *PostgreSQLRoutes) regenerateRoutes() {
	handlerRegenerate := handlerpostgres.RegenerateHandler{}
	regenerateRoute := a.mainRoute.PathPrefix("/regenerate").Subrouter()
	regenerateRoute.HandleFunc("/getProcess", handlerRegenerate.GetRegenerateProcess).Methods("GET")
	regenerateRoute.HandleFunc("/getData/{tablename}/{batchid}", handlerRegenerate.GetRegenerateData).Methods("GET")
	regenerateRoute.HandleFunc("/closeProcess", handlerRegenerate.CloseRegenerateProcess).Methods("POST")
}

// configurationRoutes : Routes of Configuration
func (a *PostgreSQLRoutes) configurationRoutes() {
	handlerConfiguration := handlerpostgres.PostgreSQLConfiguration{}
	configurationRoute := Router.PathPrefix("/postgresql/configuration").Subrouter()
	configurationRoute.HandleFunc("/getDataStructure", handlerConfiguration.GetDataStructure).Methods("GET")
	configurationRoute.HandleFunc("/getDataType", handlerConfiguration.GetDataType).Methods("GET")
	configurationRoute.HandleFunc("/getSMTPConfig", handlerConfiguration.GetSMTPConfig).Methods("GET")
}

// tapCodeRoutes : Routes of Tap Code
func (a *PostgreSQLRoutes) tapCodeRoutes() {
	handlerTapCode := handlerpostgres.TapCodeHandler{}
	configurationRoute := Router.PathPrefix("/postgresql/tapcode").Subrouter()
	configurationRoute.HandleFunc("/getTapCode", handlerTapCode.GetTapCode).Methods("GET")
}

// dataOnlyRoutes : Routes of Data Only
func (a *PostgreSQLRoutes) dataOnlyRoutes() {
	handler := handlerpostgres.DataOnlyHandler{}
	dataOnlyRoute := a.mainRoute.PathPrefix("/dataonly").Subrouter()
	dataOnlyRoute.HandleFunc("/getProcess", handler.GetDataOnlyProcess).Methods("GET")
	dataOnlyRoute.HandleFunc("/closeProcess", handler.CloseProcessDataOnly).Methods("POST")
}

// alertRoutes : Routes of Alert File
func (a *PostgreSQLRoutes) alertRoutes() {
	handler := handlerpostgres.AlertHandler{}
	alertRoute := a.mainRoute.PathPrefix("/alert").Subrouter()
	alertRoute.HandleFunc("/getProcess", handler.GetProcessAlert).Methods("GET")
	alertRoute.HandleFunc("/updateLastAlert/{datetime}", handler.UpdateLastAlert).Methods("PUT")
	alertRoute.HandleFunc("/makeTask", handler.MakeMailTask).Methods("POST")
	alertRoute.HandleFunc("/getTask", handler.GetMailTask).Methods("GET")
	alertRoute.HandleFunc("/updateStatus/{taskID}/{taskStatus}", handler.UpdateTaskStatus).Methods("PUT")
}

// reportRoutes : Routes of Report File
func (a *PostgreSQLRoutes) reportRoutes() {
	handler := handlerpostgres.ReportHandler{}

	// Daily
	reportRouteDaily := a.mainRoute.PathPrefix("/report/daily").Subrouter()
	reportRouteDaily.HandleFunc("/getProcess", handler.GetProcessReport).Methods("GET")
	reportRouteDaily.HandleFunc("/getLogMatch/{batchID}", handler.GetLogMatch).Methods("GET")
	reportRouteDaily.HandleFunc("/getLogTapinOnly/{batchID}", handler.GetLogTapinOnly).Methods("GET")
	reportRouteDaily.HandleFunc("/closeProcess", handler.CloseProcessReport).Methods("POST")

	// Monthly
	reportRouteMonthly := a.mainRoute.PathPrefix("/report/monthly").Subrouter()
	reportRouteMonthly.HandleFunc("/getProcess", handler.GetProcessReportMonthly).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogReconMonthly/{periode}/{momt}/{postpre}", handler.GetLogReconMonthly).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogCheckdup/{periode}/{momt}/{postpre}", handler.GetLogCheckDup).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogOCSOnly/{periode}/{momt}/{postpre}", handler.GetLogOCSOnly).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogRecon/{periode}/{momt}/{postpre}", handler.GetLogRecon).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogMatchToleransiGroup/{periode}/{momt}/{postpre}", handler.GetLogMatchToleransiGroup).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogReportMonthly/{periode}/{momt}/{postpre}/{datatypeid}", handler.GetLogReportMonthly).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogCheckDupPartnerID/{periode}/{momt}/{postpre}/{datatype}", handler.GetLogCheckDupPartnerID).Methods("GET")
	reportRouteMonthly.HandleFunc("/getLogCheckDupTopTen/{periode}/{momt}/{postpre}/{datatype}", handler.GetLogCheckDupTopTen).Methods("GET")
	reportRouteMonthly.HandleFunc("/closeProcess", handler.CloseProcessReportMonthly).Methods("POST")
}

// reconTapinOnlyRoutes : Routes of Recon
func (a *PostgreSQLRoutes) reconTapinOnlyRoutes() {
	handlerReconTapinOnly := handlerpostgres.ReconTapinOnlyHandler{}
	reconTapinOnlyRoute := a.mainRoute.PathPrefix("/recontapinonly").Subrouter()
	reconTapinOnlyRoute.HandleFunc("/getProcess", handlerReconTapinOnly.GetProcessReconTapinOnly).Methods("GET")
	reconTapinOnlyRoute.HandleFunc("/getListTapinOnly/{periode}/{moMt}/{postPre}", handlerReconTapinOnly.GetListTapinOnly).Methods("GET")
	reconTapinOnlyRoute.HandleFunc("/getLevelVolcomp", handlerReconTapinOnly.GetLevelVolcomp).Methods("GET")
}

// generateL3Routes : Routes of Recon
func (a *PostgreSQLRoutes) generateL3Routes() {
	currentHandler := handlerpostgres.GenL3Handler{}
	subRouter := a.mainRoute.PathPrefix("/generatel3").Subrouter()
	subRouter.HandleFunc("/getProcess", currentHandler.GetProcessGenerateL3).Methods("GET")
	subRouter.HandleFunc("/getData/{periode}/{postpre}/{momt}/{datatype}", currentHandler.GetDataL3).Methods("GET")
	subRouter.HandleFunc("/getDataMatch/{periode}/{postpre}/{momt}", currentHandler.GetDataL3Match).Methods("GET")
	subRouter.HandleFunc("/closeProcess", currentHandler.CloseProcessGenerateL3).Methods("POST")
}
