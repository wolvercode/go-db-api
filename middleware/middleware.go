package middleware

import (
	// "encoding/json"
	// "fmt"
	"net/http"
	"strings"
)

// LoggingMiddleware : Logging Middleware
func LoggingMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")

	next.ServeHTTP(w, r)
}

// CustomMiddleware : Custom Middleware
func CustomMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here

		// fmt.Println(r.Header.Get("Content-Type"))
		// m := map[string]string{}
		// decoder := json.NewDecoder(r.Body)
		// err := decoder.Decode(&m)
		// if err != nil {
		// 	panic(err)
		// }
		// fmt.Println(m)
		// Call the next handler, which can be another middleware in the chain, or the final
		next.ServeHTTP(w, r)
	})
}
