package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// ReportModel :
type ReportModel struct{}

// GetProcessReport : Function to Get Process Report
func (p *ReportModel) GetProcessReport() ([]ProcessReport, error) {
	db := app.Appl.PostgreDB

	var outProcess []ProcessReport

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_report;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessReport{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.Day, &scanProcess.StartDate, &scanProcess.DataTypeID, &scanProcess.MoMt, &scanProcess.PostPre,
			&scanProcess.FilenameSource, &scanProcess.ReportID, &scanProcess.ReportPath, &scanProcess.ReportFilename, &scanProcess.SourceInput,
			&scanProcess.SplitMO, &scanProcess.SplitMT, &scanProcess.SplitReject, &scanProcess.SplitFilename,
			&scanProcess.LookupInput, &scanProcess.LookupReject, &scanProcess.LookupOutput, &scanProcess.LookupFilename,
			&scanProcess.CekdupInput, &scanProcess.CekdupReject, &scanProcess.CekdupDuplicate, &scanProcess.CekdupUnduplicate, &scanProcess.CekdupFilename,
			&scanProcess.VolcompInput, &scanProcess.VolcompMatch, &scanProcess.VolcompTapinOnly, &scanProcess.FileTapinOnly,
			&scanProcess.RejectFieldEmpty, &scanProcess.RejectImsi, &scanProcess.RejectMOPartnerID, &scanProcess.RejectDurasi0, &scanProcess.RejectTapCode, &scanProcess.LookupRejectFile,
			&scanProcess.FilterTapCode,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// GetLogMatch :
func (p *ReportModel) GetLogMatch(batchID string) ([]StructLogMatch, error) {
	db := app.Appl.PostgreDB

	var output []StructLogMatch

	rows, err := db.Query("select * from roaming_vc_schema.sp_vc_get_log_match($1);", batchID)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogMatch{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.IDToleransi, &scanProcess.Startcall, &scanProcess.Durasi, &scanProcess.TotalMatch,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// GetLogTapinOnly :
func (p *ReportModel) GetLogTapinOnly(batchID string) ([]StructLogTapinOnly, error) {
	db := app.Appl.PostgreDB

	var output []StructLogTapinOnly

	rows, err := db.Query("select * from roaming_vc_schema.sp_vc_get_log_tapinonly($1);", batchID)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogTapinOnly{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.PartnerID, &scanProcess.TotalRecord, &scanProcess.TotalDuration,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// CloseProcessReport : Function to Close Report Process
func (p *ReportModel) CloseProcessReport(param StructCloseReport) error {
	db := app.Appl.PostgreDB

	//sp_vc_close_report(v_process_id, v_batch_id, v_processstatus_id, v_filetype_report, v_pathfile_report, v_filename_report, v_periode, v_err_msg)
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_report($1, $2, $3, $4, $5, $6, $7, $8);",
		param.ProcessConf.ProcessID, param.ProcessConf.BatchID, param.ProcessStatusID,
		param.ProcessConf.ReportID, param.ProcessConf.ReportPath, param.ProcessConf.ReportFilename,
		param.ProcessConf.Periode, param.ErrorMessage)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// GetProcessReportMonthly : Function to Get Process Report Monthly
func (p *ReportModel) GetProcessReportMonthly() ([]ProcessReportMonthly, error) {
	db := app.Appl.PostgreDB

	var outProcess []ProcessReportMonthly

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_report_monthly;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessReportMonthly{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.MoMt, &scanProcess.PostPre,
			&scanProcess.ReportID, &scanProcess.ReportPath, &scanProcess.ReportFilename,
			&scanProcess.FilterTapCode,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// GetLogReconMonthly :
func (p *ReportModel) GetLogReconMonthly(periode, moMt, postPre string) (StructLogReconMonthly, error) {
	db := app.Appl.PostgreDB

	var output StructLogReconMonthly

	rows, err := db.Query("select * from roaming_vc_schema.v_sum_l_volcomp_tapin_periode where periode = $1 and mo_mt = $2 and post_pre = $3;", periode, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}

	for rows.Next() {
		// scanProcess := StructLogReconMonthly{}

		rows.Scan(
			&output.Periode, &output.MoMt, &output.PostPre,
			&output.Input, &output.Match, &output.TapinOnly,
			&output.TapinOnlyDuration, &output.MatchDuration,
			&output.SplitInput, &output.SplitMo,
			&output.SplitMt, &output.SplitReject,
			&output.Duplicate, &output.LookupReject,
		)

		// output = append(output, scanProcess)
	}

	return output, nil
}

// GetLogCheckDup :
func (p *ReportModel) GetLogCheckDup(periode, moMt, postPre string) (map[string]StructLogCekdup, error) {
	db := app.Appl.PostgreDB

	output := make(map[string]StructLogCekdup)

	rows, err := db.Query("select * from roaming_vc_schema.v_sum_l_cekdup_periode where periode = $1 and mo_mt = $2 and post_pre = $3;", periode, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogCekdup{}

		rows.Scan(
			&scanProcess.Periode, &scanProcess.DataType, &scanProcess.MoMt, &scanProcess.PostPre,
			&scanProcess.Input, &scanProcess.Duplicate, &scanProcess.Unduplicate,
			&scanProcess.DuplicateDuration, &scanProcess.UnduplicateDuration,
		)

		output[scanProcess.DataType.String] = scanProcess
	}

	return output, nil
}

// GetLogOCSOnly :
func (p *ReportModel) GetLogOCSOnly(periode, moMt, postPre string) (StructLogOCSOnly, error) {
	db := app.Appl.PostgreDB

	var output StructLogOCSOnly

	rows, err := db.Query(`SELECT * FROM roaming_vc_schema.v_sum_l_volcomp_ocs_periode WHERE periode = $1 and mo_mt = $2 and post_pre = $3;`, periode, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}

	for rows.Next() {
		rows.Scan(
			&output.Periode, &output.MoMt, &output.PostPre,
			&output.InputRecord, &output.InputDuration,
			&output.OnlyRecord, &output.OnlyDuration,
			&output.MatchRecord, &output.MatchDuration,
			&output.SplitInput, &output.SplitMo, &output.SplitMt, &output.SplitReject,
			&output.Duplicate, &output.LookupReject,
		)
	}

	return output, nil
}

// GetLogRecon :
func (p *ReportModel) GetLogRecon(periode, moMt, postPre string) (map[string]StructLogRecon, error) {
	db := app.Appl.PostgreDB

	output := make(map[string]StructLogRecon)

	rows, err := db.Query(`SELECT * FROM roaming_vc_schema.v_sum_l_recon_periode WHERE periode = $1 and mo_mt = $2 and post_pre = $3;`, periode, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogRecon{}

		rows.Scan(
			&scanProcess.Periode, &scanProcess.PostPre, &scanProcess.MoMt, &scanProcess.PartnerID,
			&scanProcess.TapinInputRecord, &scanProcess.TapinInputDuration,
			&scanProcess.TapinOnlyRecord, &scanProcess.TapinOnlyDuration, &scanProcess.TapinOnlyRevenue,
			&scanProcess.TapinMatchRecord, &scanProcess.TapinMatchDuration, &scanProcess.TapinMatchRevenue,
			&scanProcess.OcsInputRecord, &scanProcess.OcsInputDuration,
			&scanProcess.OcsOnlyRecord, &scanProcess.OcsOnlyDuration,
			&scanProcess.OcsMatchRecord, &scanProcess.OcsMatchDuration,
			&scanProcess.OcsOnlyRevenue, &scanProcess.OcsMatchRevenue, &scanProcess.OcsInputRevenue,
		)

		output[scanProcess.PartnerID.String] = scanProcess
	}

	return output, nil
}

// GetLogMatchToleransiGroup :
func (p *ReportModel) GetLogMatchToleransiGroup(periode, moMt, postPre string) (map[string]StructLogMatchToleransiGroup, error) {
	db := app.Appl.PostgreDB

	output := make(map[string]StructLogMatchToleransiGroup)

	rows, err := db.Query(`SELECT * FROM roaming_vc_schema.v_l_match_toleransi_partnerid WHERE periode = $1 and mo_mt = $2 and post_pre = $3;`, periode, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogMatchToleransiGroup{}

		rows.Scan(
			&scanProcess.Periode, &scanProcess.PostPre, &scanProcess.MoMt, &scanProcess.PartnerID,
			&scanProcess.Record0, &scanProcess.Record30, &scanProcess.Record60, &scanProcess.Record120, &scanProcess.RecordBigger120,
			&scanProcess.Duration0, &scanProcess.Duration30, &scanProcess.Duration60, &scanProcess.Duration120, &scanProcess.DurationBigger120,
		)

		output[scanProcess.PartnerID.String] = scanProcess
	}

	return output, nil
}

// GetLogReportMonthly : Function to Get Process Report Monthly
func (p *ReportModel) GetLogReportMonthly(periode, moMt, postPre, dataTypeID string) ([]StructLogReportMonthly, error) {
	db := app.Appl.PostgreDB

	var outProcess []StructLogReportMonthly

	// perform a db.Query
	rows, err := db.Query(`SELECT * FROM roaming_vc_schema.v_data_report_monthly WHERE periode = $1 and mo_mt = $2 and post_pre = $3 and data_type_id = $4;`, periode, moMt, postPre, dataTypeID)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogReportMonthly{}
		rows.Scan(
			&scanProcess.BatchID, &scanProcess.Periode, &scanProcess.Day,
			&scanProcess.DataTypeID, &scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.FilenameCDR,
			&scanProcess.SplitInput, &scanProcess.SplitReject, &scanProcess.SplitMO, &scanProcess.SplitMT, &scanProcess.SplitFilename,
			&scanProcess.LookupInput, &scanProcess.LookupReject, &scanProcess.LookupOutput, &scanProcess.LookupFilename,
			&scanProcess.CekdupInput, &scanProcess.CekdupDuplicate, &scanProcess.CekdupUnduplicate, &scanProcess.CekdupFilename,
			&scanProcess.VolcompInput, &scanProcess.VolcompMatch, &scanProcess.VolcompTapinOnly, &scanProcess.VolcompTapinOnlyDuration, &scanProcess.FileTapinOnly,
			&scanProcess.RejectFieldEmpty, &scanProcess.RejectImsi, &scanProcess.RejectDurasi0, &scanProcess.RejectTapCode, &scanProcess.RejectMOPartnerID,
			&scanProcess.RejectImsiDurasi, &scanProcess.RejectTapCodeDurasi, &scanProcess.RejectMOPartnerIDDurasi,
			&scanProcess.LookupRejectFilename,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// GetLogCheckDupPartnerID :
func (p *ReportModel) GetLogCheckDupPartnerID(periode, moMt, postPre, dataType string) ([]StructLogCekdupPartnerID, error) {
	db := app.Appl.PostgreDB

	var output []StructLogCekdupPartnerID

	rows, err := db.Query("select * from roaming_vc_schema.v_sum_l_cekdup_periode_partnerid where periode = $1 and mo_mt = $2 and post_pre = $3 and data_type = $4;", periode, moMt, postPre, dataType)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogCekdupPartnerID{}

		rows.Scan(
			&scanProcess.Periode, &scanProcess.DataType, &scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.PartnerID,
			&scanProcess.Input, &scanProcess.Duplicate, &scanProcess.Unduplicate,
			&scanProcess.DuplicateDuration, &scanProcess.UnduplicateDuration,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// GetLogCheckDupTopTen :
func (p *ReportModel) GetLogCheckDupTopTen(periode, moMt, postPre, dataType string) ([]StructLogCekdupTopTen, error) {
	db := app.Appl.PostgreDB

	var output []StructLogCekdupTopTen

	rows, err := db.Query("select * from roaming_vc_schema.sp_vc_get_top_ten_dup($1, $2, $3, $4);", periode, postPre, moMt, dataType)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructLogCekdupTopTen{}

		rows.Scan(
			&scanProcess.Periode, &scanProcess.PostPre, &scanProcess.MoMt,
			&scanProcess.Imsi, &scanProcess.BNumber, &scanProcess.Starttime,
			&scanProcess.PartnerID, &scanProcess.RecordCount, &scanProcess.SumDuration,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// CloseProcessReportMonthly : Function to Close Report Process
func (p *ReportModel) CloseProcessReportMonthly(param StructCloseReportMonthly) error {
	db := app.Appl.PostgreDB

	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_report_monthly($1, $2, $3, $4, $5, $6, $7, $8);",
		param.ProcessConf.ProcessID, param.ProcessConf.BatchID, param.ProcessStatusID,
		param.ProcessConf.ReportID, param.ProcessConf.ReportPath, param.ProcessConf.ReportFilename,
		param.ProcessConf.Periode, param.ErrorMessage)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
