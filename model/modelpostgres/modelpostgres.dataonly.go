package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// DataOnlyModel :
type DataOnlyModel struct{}

// GetDataOnlyProcess : Function to Get Process Regenerate
func (p *DataOnlyModel) GetDataOnlyProcess() ([]ProcessDataOnly, error) {
	db := app.Appl.PostgreDB

	var listProcess []ProcessDataOnly

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_data_only;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessDataOnly{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.DataTypeID, &scanProcess.MoMt,
			&scanProcess.PostPre, &scanProcess.FileTypeDB, &scanProcess.PathDB, &scanProcess.MatchDB, &scanProcess.FileTypeDataOnly,
			&scanProcess.PathDataOnly, &scanProcess.FilenameDataOnly,
		)

		listProcess = append(listProcess, scanProcess)
	}

	return listProcess, nil
}

// CloseProcessDataOnly : Function to Close DataOnly Proccess
func (p *DataOnlyModel) CloseProcessDataOnly(param StructCloseDataOnly) error {
	db := app.Appl.PostgreDB
	// perform a db.Query
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_data_only($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15);",
		param.ProcessConf.ProcessID, param.ProcessConf.BatchID, param.ProcessConf.DataTypeID, param.ProcessConf.FileTypeDataOnly,
		param.ProcessConf.PathDataOnly, param.ProcessConf.FilenameDataOnly, param.ProcessConf.Periode, param.ProcessStatusID, param.ErrorMessage,
		param.CountInputRecord, param.CountInputDuration,
		param.CountOnlyRecord, param.CountOnlyDuration,
		param.CountMatchRecord, param.CountMatchDuration,
	)
	// if there is an error, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	// Inserting Log PartnerID
	if len(param.LogPartnerID) > 0 {
		query := "INSERT INTO roaming_vc_schema.vc_l_data_only_partnerid (process_id,partner_id,input_record,input_duration,only_record,only_duration,match_record,match_duration,input_revenues,only_revenues,match_revenues) VALUES"
		for partnerID := range param.LogPartnerID {
			query = fmt.Sprintf("%s ('%s','%s',%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f),", query, param.ProcessConf.ProcessID, partnerID,
				param.LogPartnerID[partnerID].RecordsInput, param.LogPartnerID[partnerID].DurationsInput,
				param.LogPartnerID[partnerID].RecordsOnly, param.LogPartnerID[partnerID].DurationsOnly,
				param.LogPartnerID[partnerID].RecordsMatch, param.LogPartnerID[partnerID].DurationsMatch,
				param.LogPartnerID[partnerID].RevenuesInput, param.LogPartnerID[partnerID].RevenuesOnly, param.LogPartnerID[partnerID].RevenuesMatch,
			)
		}

		query = query[0:len(query)-1] + ";"

		statement, err = db.Query(query)
		// if there is an error inserting, handle it
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		statement.Close()
	}

	return nil
}
