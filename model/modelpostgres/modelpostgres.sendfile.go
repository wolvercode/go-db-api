package modelpostgres

import (
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/billing/go-db-api/app"
)

// SendFileModel :
type SendFileModel struct{}

// GetProcessSendFile : Function to Get Process SendFile
func (p *SendFileModel) GetProcessSendFile() ([]ProcessSendFile, error) {
	db := app.Appl.PostgreDB

	var outProcess []ProcessSendFile

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_sendfile;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessSendFile{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.DataTypeID, &scanProcess.DayPeriode,
			&scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.FileTypeID, &scanProcess.PathFile, &scanProcess.Filename,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// GetReceiverSendFile : Function to Get Receiver SendFile
func (p *SendFileModel) GetReceiverSendFile() (ReceiverSendFile, error) {
	db := app.Appl.PostgreDB

	var outProcess ReceiverSendFile

	// perform a db.Query
	statement := db.QueryRow("Select VALUESYSCONF from roaming_vc_schema.VC_P_SYSCONF where SYSCONF = 'TAPIN_RECEIVER';")

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	columns := strings.Split(vresult, "|")
	outProcess.Host = columns[0]
	outProcess.User = columns[1]
	outProcess.Pass = columns[2]
	outProcess.Path = columns[3]

	if vresult == "" {
		return outProcess, errors.New("Cannot Create Table")
	}

	return outProcess, nil
}

// CloseProcessSendFile : Function to Close SendFile Proccess
func (p *SendFileModel) CloseProcessSendFile(param StructCloseSendFile) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert

	statement, err := db.Query("select roaming_vc_schema.sp_vc_close_sendfile($1,$2,$3,$4,$5,$6,$7);",
		param.ProcessID, param.BatchID, param.Periode, param.DayPeriode, param.DataTypeID, param.ProcessStatusID, param.ErrorMessage)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
