package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// ProcessModel :
type ProcessModel struct{}

// UpdateStatusProcess : Function to Update Proccess Status
func (p *ProcessModel) UpdateStatusProcess(processID, processStatusID string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("Update roaming_vc_schema.vc_t_process Set processstatus_id = $1, startdate = now() Where process_id = $2;", processStatusID, processID)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// GetSlicedProcess : Function to Get Slice Process
func (p *ProcessModel) GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre string) ([]SlicedProcess, error) {
	db := app.Appl.PostgreDB

	var slicedProcess []SlicedProcess

	rows, err := db.Query("select * from roaming_vc_schema.sp_vc_get_sliced_process($1, $2, $3, $4, $5);", dataTypeID, minStartcall, maxStartcall, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := SlicedProcess{}
		rows.Scan(&scanProcess.PathDB, &scanProcess.Periode)

		if scanProcess.PathDB != "" {
			slicedProcess = append(slicedProcess, scanProcess)
		}
	}

	return slicedProcess, nil
}
