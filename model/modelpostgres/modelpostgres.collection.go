package modelpostgres

import (
	"errors"
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// CollectionModel :
type CollectionModel struct{}

// SSTarget :
type SSTarget struct {
	FileTypeID  string `json:"filetype_id"`
	DataTypeID  string `json:"datatype_id"`
	FileType    string `json:"filetype"`
	PathFile    string `json:"pathfile"`
	Description string `json:"description"`
}

// GetSSHost : Function to Get Collection SSH Host
func (p *CollectionModel) GetSSHost() (string, error) {
	db := app.Appl.PostgreDB

	// perform a db.Query
	statement := db.QueryRow("Select VALUESYSCONF from roaming_vc_schema.VC_P_SYSCONF where SYSCONF = 'sshost';")

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	if vresult == "" {
		return "null", nil
	}

	return vresult, nil
}

// GetSSUser : Function to Get Collection SSH User
func (p *CollectionModel) GetSSUser() (string, error) {
	db := app.Appl.PostgreDB

	// perform a db.Query
	statement := db.QueryRow("Select VALUESYSCONF from roaming_vc_schema.VC_P_SYSCONF where SYSCONF = 'ssuser';")

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	if vresult == "" {
		return "null", nil
	}

	return vresult, nil
}

// GetSSPass : Function to Get Collection SSH Pass
func (p *CollectionModel) GetSSPass() (string, error) {
	db := app.Appl.PostgreDB

	// perform a db.Query
	statement := db.QueryRow("Select VALUESYSCONF from roaming_vc_schema.VC_P_SYSCONF where SYSCONF = 'sspass';")

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	if vresult == "" {
		return "null", nil
	}

	return vresult, nil
}

// GetSSHome : Function to Get Collection SSH Home
func (p *CollectionModel) GetSSHome() (string, error) {
	db := app.Appl.PostgreDB

	// perform a db.Query
	statement := db.QueryRow("Select VALUESYSCONF from roaming_vc_schema.VC_P_SYSCONF where SYSCONF = 'sshome';")

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	if vresult == "" {
		return "null", nil
	}

	return vresult, nil
}

// GetPathByFiletype : Function to Get Collection Path File
func (p *CollectionModel) GetPathByFiletype(filetypeid string) (string, error) {
	db := app.Appl.PostgreDB

	// perform a db.Query
	statement := db.QueryRow("Select PATHFILE from roaming_vc_schema.VC_P_FILETYPE where FILETYPE_ID = $1;", filetypeid)

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	if vresult == "" {
		return "null", nil
	}

	return vresult, nil
}

// GetSSTarget : Function to Get Collection SSH Target
func (p *CollectionModel) GetSSTarget(filetypeName, datatypeid string) (SSTarget, error) {
	db := app.Appl.PostgreDB

	var scanProcess SSTarget

	// perform a db.Query
	statement := db.QueryRow("SELECT * FROM roaming_vc_schema.VC_P_FILETYPE WHERE FILETYPE = $1 AND DATA_TYPE_ID = $2;", filetypeName, datatypeid)

	// be careful deferring Queries if you are using transactions
	statement.Scan(&scanProcess.FileTypeID, &scanProcess.DataTypeID, &scanProcess.FileType, &scanProcess.PathFile, &scanProcess.Description)

	if scanProcess.FileTypeID == "" {
		return scanProcess, errors.New("Cannot Get SSH Target")
	}

	return scanProcess, nil
}

// CloseProcessCollection : Function to Close Collection
func (p *CollectionModel) CloseProcessCollection(param StructCloseCollection) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("select roaming_vc_schema.sp_vc_close_collection($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);",
		param.Periode, param.DataTypeID, param.TargetDir, param.Filetype, param.FilenameInCtrl, param.Day,
		param.FlagImsi, param.ProcessStatusID, param.ErrorMessage, param.PostPre,
	)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
