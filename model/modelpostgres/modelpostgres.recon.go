package modelpostgres

import (
	"fmt"
	"strings"

	"bitbucket.org/billing/go-db-api/app"
)

// ReconModel :
type ReconModel struct{}

// GetProcessRecon : Function to Get Process Recon
func (p *ReconModel) GetProcessRecon() ([]ProcessRecon, error) {
	db := app.Appl.PostgreDB

	var processRecon []ProcessRecon

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_rekon;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessRecon{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.ProcesstypeID, &scanProcess.BatchID, &scanProcess.DataTypeID, &scanProcess.Periode,
			&scanProcess.DayPeriode, &scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.UndupID, &scanProcess.PathUndup, &scanProcess.FilenameUndup,
			&scanProcess.OnlyID, &scanProcess.PathOnly, &scanProcess.FilenameOnly,
			&scanProcess.MatchID, &scanProcess.PathMatch, &scanProcess.FilenameMatch,
			&scanProcess.MaxDelay, &scanProcess.MinStartcall, &scanProcess.MaxStartcall,
		)

		processRecon = append(processRecon, scanProcess)
	}

	return processRecon, nil
}

// CloseProcessRecon : Function to Close Recon Proccess
func (p *ReconModel) CloseProcessRecon(param StructCloseRecon) error {
	db := app.Appl.PostgreDB

	// perform a db.Query insert
	//v_process_id, v_batch_id, v_period, v_day, v_data_type_id, v_volcomp_input, v_volcomp_match, v_volcomp_tapin_only, v_file_type_match, v_path_file_match, v_file_name_match, v_file_type_tapinonly, v_path_file_tapinonly, v_file_name_tapinonly
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_rekon($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16);",
		param.ProcessConf.ProcessID, param.ProcessConf.BatchID, param.ProcessConf.Periode, param.ProcessConf.DayPeriode, param.ProcessConf.DataTypeID,
		param.TotalInput, param.TotalMatch, param.TotalTapinOnly, param.ProcessConf.MatchID, param.ProcessConf.PathMatch,
		param.ProcessConf.FilenameMatch, param.ProcessConf.OnlyID, param.ProcessConf.PathOnly, param.ProcessConf.FilenameOnly,
		param.ProcessStatusID, param.ErrorMessage)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	statement.Close()

	// Inserting Log Match
	if len(param.LogMatch) > 0 {
		logQuery := "INSERT INTO roaming_vc_schema.vc_l_volcomp_match (process_id,id_toleransi,volcomp_match,total_duration) VALUES"
		for idToleransi := range param.LogMatch {
			logQuery = fmt.Sprintf("%s (%s,%d,%d,%d),", logQuery, param.ProcessConf.ProcessID, idToleransi, param.LogMatch[idToleransi]["records"], param.LogMatch[idToleransi]["durations"])
		}
		logQuery = logQuery[0:len(logQuery)-1] + ";"

		statement, err = db.Query(logQuery)
		// if there is an error inserting, handle it
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		statement.Close()
	}

	// Inserting Log TAPIN ONLY
	if len(param.LogTapinOnly) > 0 {
		queryLogTapinOnly := "INSERT INTO roaming_vc_schema.vc_l_volcomp_tapinonly (process_id,partner_id,total_record,total_duration,total_revenue) VALUES"
		for partnerID := range param.LogTapinOnly {
			queryLogTapinOnly = fmt.Sprintf("%s ('%s','%s',%d,%d,%.2f),", queryLogTapinOnly, param.ProcessConf.ProcessID, partnerID,
				param.LogTapinOnly[partnerID].Records, param.LogTapinOnly[partnerID].Durations, param.LogTapinOnly[partnerID].Revenues)
		}

		queryLogTapinOnly = queryLogTapinOnly[0:len(queryLogTapinOnly)-1] + ";"

		statement, err = db.Query(queryLogTapinOnly)
		// if there is an error inserting, handle it
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		statement.Close()
	}

	// Inserting Log Match PartnerID
	if len(param.LogMatchPartnerID) > 0 {
		query := "INSERT INTO roaming_vc_schema.vc_l_volcomp_match_partnerid (process_id,partner_id,total_match,total_duration,total_revenue) VALUES"
		for partnerID := range param.LogMatchPartnerID {
			query = fmt.Sprintf("%s ('%s','%s',%d,%d,%.2f),", query, param.ProcessConf.ProcessID, partnerID,
				param.LogMatchPartnerID[partnerID].Records, param.LogMatchPartnerID[partnerID].Durations, param.LogMatchPartnerID[partnerID].Revenues)
		}

		query = query[0:len(query)-1] + ";"

		statement, err = db.Query(query)
		// if there is an error inserting, handle it
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		statement.Close()
	}

	// Inserting Log Match Toleransi Partner ID
	if len(param.LogMatchToleransiPartnerID) > 0 {
		logQuery := "INSERT INTO roaming_vc_schema.vc_l_volcomp_match_tol_partnerid (process_id,partner_id,id_toleransi,total_record,total_duration,total_revenue) VALUES"
		for partnerID := range param.LogMatchToleransiPartnerID {
			for idToleransi := range param.LogMatchToleransiPartnerID[partnerID] {
				logQuery = fmt.Sprintf("%s ('%s','%s',%d,%d,%d,%.2f),", logQuery, param.ProcessConf.ProcessID, partnerID, idToleransi,
					param.LogMatchToleransiPartnerID[partnerID][idToleransi].Records, param.LogMatchToleransiPartnerID[partnerID][idToleransi].Durations, param.LogMatchToleransiPartnerID[partnerID][idToleransi].Revenues)
			}
		}
		logQuery = logQuery[0:len(logQuery)-1] + ";"

		statement, err = db.Query(logQuery)
		// if there is an error inserting, handle it
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		statement.Close()
	}

	return nil
}

// GetLevelVolcomp : Function to Get Level of Volcomp
func (p *ReconModel) GetLevelVolcomp() ([]LevelVolcomp, error) {
	db := app.Appl.PostgreDB

	var levelVolcomp []LevelVolcomp

	rows, err := db.Query("select level from roaming_vc_schema.vc_t_level_volcomp GROUP BY level order by level;")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	for rows.Next() {
		scanProcess := LevelVolcomp{}

		rows.Scan(&scanProcess.Level)

		levelVolcompConf := map[string]LevelVolcompConf{}
		rowsConf, err := db.Query("select * from roaming_vc_schema.vc_t_level_volcomp WHERE level = $1 ORDER BY id_level ASC;", scanProcess.Level)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		for rowsConf.Next() {
			scanProcess := LevelVolcompConf{}
			var scanParameterCompare string
			var arParameterCompare []string

			var scanParameterToleransi string
			var arParameterToleransi []string
			rowsConf.Scan(&scanProcess.IDLevel, &scanProcess.Level, &scanProcess.Type, &scanParameterCompare, &scanParameterToleransi)

			columns := strings.Split(scanParameterCompare, "|")
			for _, v := range columns {
				var urutan string
				err := db.QueryRow("SELECT urutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 AND fieldname = $2", scanProcess.Type, v).Scan(&urutan)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}
				arParameterCompare = append(arParameterCompare, urutan)
			}

			columns = strings.Split(scanParameterToleransi, "|")
			for _, v := range columns {
				var urutan string
				err := db.QueryRow("SELECT urutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 AND fieldname = $2", scanProcess.Type, v).Scan(&urutan)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}
				arParameterToleransi = append(arParameterToleransi, urutan)
			}

			scanProcess.ParameterCompare = arParameterCompare
			scanProcess.ParameterToleransi = arParameterToleransi

			levelVolcompConf[scanProcess.Type] = scanProcess
		}

		toleransiVolcomp := []ToleransiVolcomp{}
		// rowsToleransi, err := db.Query("select * from roaming_vc_schema.vc_t_conf_volcomp WHERE level = $1 ORDER BY priorities ASC;", scanProcess.Level)
		rowsToleransi, err := db.Query("select a.* from roaming_vc_schema.vc_t_conf_volcomp a "+
			"left join roaming_vc_schema.vc_p_sysconf b on b.sysconf = 'TOLERANSI_ACTIVE_ID' "+
			"WHERE level = $1 and a.active_id = b.valuesysconf::int "+
			"ORDER BY priorities ASC;", scanProcess.Level)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		for rowsToleransi.Next() {
			scanToleransi := ToleransiVolcomp{}
			rowsToleransi.Scan(&scanToleransi.IDToleransi, &scanToleransi.Level, &scanToleransi.Startcall,
				&scanToleransi.Durasi, &scanToleransi.Priorities, &scanToleransi.ActiveID)
			toleransiVolcomp = append(toleransiVolcomp, scanToleransi)
		}

		scanProcess.Toleransi = toleransiVolcomp
		scanProcess.Conf = levelVolcompConf
		levelVolcomp = append(levelVolcomp, scanProcess)
	}

	return levelVolcomp, nil
}
