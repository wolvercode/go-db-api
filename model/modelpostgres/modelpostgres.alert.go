package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// AlertModel :
type AlertModel struct{}

// GetProcessAlert : Function to Get Process Alert
func (p *AlertModel) GetProcessAlert() ([]ProcessAlert, error) {
	db := app.Appl.PostgreDB

	var outProcess []ProcessAlert

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_alert;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessAlert{}
		rows.Scan(
			&scanProcess.AlertID, &scanProcess.PathFile, &scanProcess.FilePattern, &scanProcess.ReceiverEmail, &scanProcess.SourcePath,
			&scanProcess.MailConfID, &scanProcess.MailConfTitle, &scanProcess.MailConfSubject, &scanProcess.MailConfMessage, &scanProcess.AlertExec, &scanProcess.LastAlert,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// UpdateLastAlert : Function to Update Last Alert
func (p *AlertModel) UpdateLastAlert(inpDateTime string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query Update
	statement, err := db.Query("Update roaming_vc_schema.vc_p_sysconf Set valuesysconf = $1 Where sysconf = 'LAST_ALERT';", inpDateTime)
	// if there is an error Updating, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// MakeMailTask : Function to Make Mail Task
func (p *AlertModel) MakeMailTask(param MailTask) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("select roaming_vc_schema.sp_vc_make_mail_task($1,$2,$3,$4,$5,$6,$7);",
		param.MailConfID, param.MailTaskTitle, param.MailTaskSubject, param.MailTaskMessage, param.MailTaskAttachment, param.MailtaskSendDate, param.ProcessID)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// GetMailTask : Function to Get Mail Task
func (p *AlertModel) GetMailTask() ([]MailTaskProcess, error) {
	db := app.Appl.PostgreDB

	var outProcess []MailTaskProcess

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_mail_task;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := MailTaskProcess{}
		rows.Scan(
			&scanProcess.MailTaskID, &scanProcess.MailConfID, &scanProcess.MailTaskTittle,
			&scanProcess.MailTaskSubject, &scanProcess.MailTaskMessage, &scanProcess.MailTaskAttachment,
			&scanProcess.MailTaskSendDate, &scanProcess.MailTaskStatus, &scanProcess.ProcessID,
			&scanProcess.ReceiverEmail,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// UpdateTaskStatus : Function to Update Task Status
func (p *AlertModel) UpdateTaskStatus(taskID, taskStatus string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("Update roaming_vc_schema.vc_t_mail_task Set mailtask_status = $1 Where mailtask_id = $2;", taskStatus, taskID)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
