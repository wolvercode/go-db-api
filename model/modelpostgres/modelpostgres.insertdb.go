package modelpostgres

import (
	"bytes"
	"errors"
	"fmt"
	"os/exec"
	"strings"

	"bitbucket.org/billing/go-db-api/app"
)

// InsertDBModel :
type InsertDBModel struct{}

// GetProcessInsertDB : Function to Get Process Insert DB
func (p *InsertDBModel) GetProcessInsertDB() ([]ProcessInsertDB, error) {
	db := app.Appl.PostgreDB

	var outProcess []ProcessInsertDB

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_insertdb;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessInsertDB{}
		rows.Scan(&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.ProcessTypeID, &scanProcess.Periode, &scanProcess.DataTypeID,
			&scanProcess.DayPeriode, &scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.InputID, &scanProcess.PathInput, &scanProcess.FilenameInput)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// InsertOCSToDBOld : Function to Insert Data OCS to DB
func (p *InsertDBModel) InsertOCSToDBOld(modelData *DataOCSold) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("INSERT INTO roaming_vc_schema.vc_d_ocs_post VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37, $38, $39, $40, $41, $42, $43, $44, $45, $46, $47, $48, $49, $50, $51, $52, $53, $54, $55, $56, $57, $58, $59, $60, $61, $62, $63, $64, $65, $66, $67, $68, $69, $70, $71, $72, $73, $74, $75, $76, $77, $78, $79, $80)",
		modelData.BatchID, modelData.Edtm, modelData.Aparty, modelData.Locationnumbertypea,
		modelData.Locationnumbera, modelData.Bparty, modelData.Locationnumbertypeb, modelData.Locationnumberb, modelData.Nrofreroutings, modelData.Imsi,
		modelData.Nrofnetworkreroutings, modelData.Redirectingpartyid, modelData.Callduration1, modelData.Callduration, modelData.Chargingid,
		modelData.Roamingflag, modelData.Vlrnumber, modelData.Cellid, modelData.Negotiatedqos, modelData.Requestedqos, modelData.Subscribedqos,
		modelData.Dialledapn, modelData.Eventtype, modelData.Providerid, modelData.Currency, modelData.Subscriberstatus, modelData.Forwardingflag,
		modelData.Firstcallflag, modelData.Camelroaming, modelData.Timezone, modelData.Accountbalance, modelData.Accountdelta, modelData.Accountnumber,
		modelData.Accountowner, modelData.Eventsource, modelData.Guidingresourcetype, modelData.Roundedduration, modelData.Totalvolume,
		modelData.Roundedtotalvolume, modelData.Eventstartperiod, modelData.Chargeincludingallowance, modelData.Discountamount, modelData.Freetotalvolume,
		modelData.Freeduration, modelData.Calldirection, modelData.Chargecode, modelData.Specialnumberind, modelData.Bonusinformation,
		modelData.Internalcause, modelData.Basiccause, modelData.Timeband, modelData.Calltype, modelData.Bonusconsumed, modelData.VasCode,
		modelData.ServiceFilter, modelData.NationalCallingZone, modelData.NationalCalledZone, modelData.IntCallingZone, modelData.IntCalledZone,
		modelData.Creditindicator, modelData.EventID, modelData.Accesscode, modelData.Countryname, modelData.Cpname, modelData.Contentid,
		modelData.Ratinggroup, modelData.Bftindicator, modelData.Unappliedamount, modelData.Partitionid, modelData.Rateddatetime, modelData.Area,
		modelData.Costband, modelData.Ratingofferid, modelData.Settlementindicator, modelData.TapCode, modelData.Mccmnc, modelData.Ratingpricingitemid,
		modelData.Fileidentifier, modelData.Recordnumber, modelData.Futurestring)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// InsertOCSToDB : Function to Insert Data OCS to DB
func (p *InsertDBModel) InsertOCSToDB(modelData *DataOCS, periode string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("INSERT INTO roaming_vc_schema.vc_d_ocs_post_"+periode+" (batch_id, edtm, aparty, bparty, imsi, callduration, roamingflag, camelroaming, "+
		"accountdelta, calltype, service_filter, tap_code) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
		modelData.BatchID, modelData.Edtm, modelData.Aparty, modelData.Bparty, modelData.Imsi, modelData.Callduration, modelData.Roamingflag,
		modelData.Camelroaming, modelData.Accountdelta, modelData.Calltype, modelData.ServiceFilter, modelData.TapCode)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// InsertTAPINToDB : Function to Insert Data TAPIN to DB
func (p *InsertDBModel) InsertTAPINToDB(modelData *DataTAPIN, periode string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("INSERT INTO roaming_vc_schema.vc_d_tapin_"+periode+" (batch_id, camel_service_key, imsi, msisdn, othertelnum, calleddigit, partnerid, local_time, starttime, "+
		"dealtime, duration, cdr_type, servicename, tap_filename, tap_fee, utc_time, flag_post_pre, plus_utc_time) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)",
		modelData.BatchID, modelData.CamelServiceKey, modelData.Imsi, modelData.Msisdn, modelData.Othertelnum, modelData.Calleddigit,
		modelData.Partnerid, modelData.LocalTime, modelData.Starttime, modelData.Dealtime, modelData.Duration, modelData.CdrType,
		modelData.Servicename, modelData.TapFilename, modelData.TapFee, modelData.UtcTime, modelData.FlagPostPre, modelData.PlusUtcTime)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// InsertTAPINOnlyToDB : Function to Insert Data TAPIN Only to DB
func (p *InsertDBModel) InsertTAPINOnlyToDB(modelData *DataTAPINOnly, periode string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("INSERT INTO roaming_vc_schema.vc_d_tapin_only_"+periode+" (batch_id, event_source, event_type_id, starttime, reserve1, reserve2, reserve3, reserve4, reserve5, reserve6, "+
		"reserve7, reserve8, reserve9, reserve10, called_number, operator_id1, call_type, charge, calling_number, reserve11, tapdecimalplaces, dialed_number, duration, msisdn, "+
		"session_id, call_forward_indicator, roaming_type_indicator, operator_id2, local_timestamp, reserve12, reserve13, reserve14, reserve15, utc_time_offset, reserve16, "+
		"reserve17, reserve18, reserve19, tapfilename) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, "+
		"$20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37, $38, $39)",
		modelData.BatchID, modelData.Eventsource, modelData.Eventtypeid, modelData.Starttime, modelData.Reserve1, modelData.Reserve2,
		modelData.Reserve3, modelData.Reserve4, modelData.Reserve5, modelData.Reserve6, modelData.Reserve7, modelData.Reserve8, modelData.Reserve9,
		modelData.Reserve10, modelData.Callednumber, modelData.Operatorid1, modelData.Calltype, modelData.Charge, modelData.Callingnumber,
		modelData.Reserve11, modelData.Tapdecimalplaces, modelData.Dialednumber, modelData.Duration, modelData.Msisdn, modelData.Sessionid,
		modelData.Callforwardindicator, modelData.Roamingtypeindicator, modelData.Operatorid2, modelData.Localtimestamp, modelData.Reserve12,
		modelData.Reserve13, modelData.Reserve14, modelData.Reserve15, modelData.Utctimeoffset, modelData.Reserve16, modelData.Reserve17,
		modelData.Reserve18, modelData.Reserve19, modelData.Tapfilename)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// CopyOCSToDB :
func (p *InsertDBModel) CopyOCSToDB(inputModel *StructCopyToTable, postPre, fileType string) error {
	cmdCopy := `psql "` + app.Appl.ConnString + `" -c "\copy roaming_vc_schema.vc_d_ocs_` + strings.ToLower(fileType) + `_` + postPre + `_` + inputModel.Periode +
		`(batch_id, edtm, aparty, bparty, imsi, callduration, roamingflag, camelroaming, accountdelta, calltype, service_filter, tap_code, cellid, mo_mt)` +
		`from '` + inputModel.Filename + `' with (FORMAT CSV, delimiter '|', FORCE_NULL(callduration))"`

	cmd := exec.Command("/bin/bash", "-c", cmdCopy)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		return errors.New(stderr.String())
	}

	return nil
}

// CopyTapinToDB :
func (p *InsertDBModel) CopyTapinToDB(inputModel *StructCopyToTable, postPre, fileType string) error {
	cmdCopy := `psql "` + app.Appl.ConnString + `" -c "\copy roaming_vc_schema.vc_d_tapin_` + strings.ToLower(fileType) + `_` + postPre + `_` + inputModel.Periode +
		`(batch_id, camel_service_key, imsi, msisdn, othertelnum, calleddigit, partnerid, local_time, starttime,` +
		`dealtime, duration, cdr_type, servicename, tap_filename, tap_fee, utc_time, plus_utc_time, mo_mt)` +
		`from '` + inputModel.Filename + `' with (FORMAT CSV, delimiter '|', FORCE_NULL(camel_service_key, imsi, msisdn, othertelnum, calleddigit, local_time, starttime, dealtime, duration, plus_utc_time))"`

	cmd := exec.Command("/bin/bash", "-c", cmdCopy)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		return errors.New(stderr.String())
	}

	return nil
}

// CopyTapinOnlyToDB :
func (p *InsertDBModel) CopyTapinOnlyToDB(inputModel *StructCopyToTable, postPre, fileType string) error {
	cmdCopy := `psql "` + app.Appl.ConnString + `" -c "\copy roaming_vc_schema.vc_d_tapin_` + strings.ToLower(fileType) + `_` + postPre + `_` + inputModel.Periode +
		`(batch_id, event_source, event_type_id, starttime, reserve1, reserve2, reserve3, reserve4, reserve5, reserve6, reserve7, reserve8, reserve9, reserve10,` +
		`called_number, operator_id1, call_type, charge, calling_number, reserve11, tapdecimalplaces, dialed_number, duration, msisdn, session_id,` +
		`call_forward_indicator, roaming_type_indicator, operator_id2, local_timestamp, reserve12, reserve13, reserve14, reserve15, utc_time_offset,` +
		`reserve16, reserve17, reserve18, reserve19, tapfilename, mo_mt) from '` + inputModel.Filename + `' with (FORMAT CSV, delimiter ',')"`

	cmd := exec.Command("/bin/bash", "-c", cmdCopy)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		return errors.New(stderr.String())
	}

	return nil
}

// CopyMatchToDB :
func (p *InsertDBModel) CopyMatchToDB(inputModel *StructCopyToTable, postPre, fileType string) error {
	cmdCopy := `psql "` + app.Appl.ConnString + `" -c "\copy roaming_vc_schema.vc_d_` + strings.ToLower(fileType) + `_` + postPre + `_` + inputModel.Periode +
		`(batch_id, camel_service_key, imsi, msisdn, othertelnum, calleddigit, partnerid, local_time, starttime, dealtime, duration,` +
		`cdr_type, servicename, tap_filename, tap_fee, utc_time, plus_utc_time, edtm, aparty, bparty, imsi_ocs, callduration,` +
		`roamingflag, camelroaming, accountdelta, calltype, service_filter, tap_code, cellid, tol_startcall, tol_durasi, mo_mt)` +
		`from '` + inputModel.Filename + `' with (FORMAT CSV, delimiter '|', FORCE_NULL(camel_service_key, imsi, msisdn, othertelnum, calleddigit, local_time, starttime, dealtime, duration, plus_utc_time,callduration,imsi_ocs))"`

	cmd := exec.Command("/bin/bash", "-c", cmdCopy)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		return errors.New(stderr.String())
	}

	return nil
}

// CreateTableIfNotExists :
func (p *InsertDBModel) CreateTableIfNotExists(periode, dataType, fileType, postPre string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement := db.QueryRow("select roaming_vc_schema.sp_vc_create_table($1,$2,$3,$4);", periode, dataType, fileType, postPre)

	// be careful deferring Queries if you are using transactions
	var vresult string
	statement.Scan(&vresult)

	if vresult == "" {
		return errors.New("Cannot Create Table")
	}

	return nil
}

// CloseProcessInsertDB : Function to Close InsertDB Proccess
func (p *InsertDBModel) CloseProcessInsertDB(param StructCloseInsertDB) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("select roaming_vc_schema.sp_vc_close_insertdb($1,$2,$3);", param.ProcessID, param.ProcessStatusID, param.ErrorMessage)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// GetTaskInsertDB : Function to Get Process Insert DB
func (p *InsertDBModel) GetTaskInsertDB() ([]TaskInsertDB, error) {
	db := app.Appl.PostgreDB

	var outProcess []TaskInsertDB

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_task_insertdb;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := TaskInsertDB{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.DataType, &scanProcess.PostPre,
			&scanProcess.MoMt, &scanProcess.InputID, &scanProcess.FileType, &scanProcess.PathInput, &scanProcess.FilenameInput,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// UpdateStatusProcess : Function to Update Proccess Status
func (p *InsertDBModel) UpdateStatusProcess(processID, processStatusID, errMsg string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("Update roaming_vc_schema.vc_t_insertdb_task Set insertdbtask_status = $1, error_msg = $2 Where process_id = $3;", processStatusID, errMsg, processID)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
