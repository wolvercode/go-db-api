package modelpostgres

import null "gopkg.in/guregu/null.v3"

// StructCloseCollection :
type StructCloseCollection struct {
	Periode         int    `json:"periode"`
	DataTypeID      int    `json:"data_type_id"`
	TargetDir       string `json:"target_dir"`
	Filetype        int    `json:"filetype"`
	FilenameInCtrl  string `json:"filename_in_ctrl"`
	Day             string `json:"day"`
	FlagImsi        int    `json:"flag_imsi"`
	ProcessStatusID int    `json:"process_status_id"`
	ErrorMessage    string `json:"error_message"`
	PostPre         string `json:"post_pre"`
}

// ProcessSplit :
type ProcessSplit struct {
	ProcessID      string `json:"process_id"`
	ProcesstypeID  string `json:"processtype_id"`
	BatchID        string `json:"batch_id"`
	DataTypeID     int    `json:"data_type_id"`
	Periode        string `json:"periode"`
	DayPeriode     string `json:"day"`
	FlagImsi       int    `json:"flag_imsi"`
	CdrID          int    `json:"cdr_id"`
	PathCdr        string `json:"path_cdr"`
	FilenameCdr    string `json:"filename_cdr"`
	OutID          int    `json:"out_id"`
	MoPath         string `json:"mo_path"`
	MoFilename     string `json:"mo_filename"`
	MtPath         string `json:"mt_path"`
	MtFilename     string `json:"mt_filename"`
	RejectID       int    `json:"reject_id"`
	RejectPath     string `json:"reject_path"`
	RejectFilename string `json:"reject_filename"`
}

// ClosingSplit : Structure of Closing Process Split
type ClosingSplit struct {
	StructProcess   ProcessSplit `json:"process_conf"`
	ProcessStatusID string       `json:"processstatus_id"`
	ErrorMessage    string       `json:"error_message"`
	TotalInput      int          `json:"total_input"`
	TotalMo         int          `json:"total_mo"`
	TotalMt         int          `json:"total_mt"`
	TotalReject     int          `json:"total_reject"`
}

// ProcessLookup :
type ProcessLookup struct {
	ProcessID       string `json:"process_id"`
	ProcesstypeID   string `json:"processtype_id"`
	BatchID         string `json:"batch_id"`
	DataTypeID      int    `json:"data_type_id"`
	Periode         string `json:"periode"`
	DayPeriode      string `json:"day"`
	FlagImsi        int    `json:"flag_imsi"`
	MoMt            string `json:"mo_mt"`
	CdrID           int    `json:"cdr_id"`
	PathCdr         string `json:"path_cdr"`
	FilenameCdr     string `json:"filename_cdr"`
	DBImsiID        int    `json:"dbimsi_id"`
	DBImsiPath      string `json:"dbimsi_path"`
	DBTapCodeID     int    `json:"dbtapcode_id"`
	DBTapCodePath   string `json:"dbtapcode_path"`
	OutID           int    `json:"out_id"`
	OutPath         string `json:"out_path"`
	OutFilename     string `json:"out_filename"`
	OutPathPost     string `json:"out_path_post"`
	OutFilenamePost string `json:"out_filename_post"`
	OutPathPre      string `json:"out_path_pre"`
	OutFilenamePre  string `json:"out_filename_pre"`
	RejectID        int    `json:"reject_id"`
	RejectPath      string `json:"reject_path"`
	RejectFilename  string `json:"reject_filename"`
	DBAnumID        int    `json:"dbanum_id"`
	DBAnumPath      string `json:"dbanum_path"`
}

// StructCloseLookup :
type StructCloseLookup struct {
	StructProcess           ProcessLookup `json:"process_conf"`
	ProcessStatusID         string        `json:"processstatus_id"`
	ErrorMessage            string        `json:"error_message"`
	LookupInput             int           `json:"lookup_input"`
	LookupReject            int           `json:"lookup_reject"`
	LookupOutput            int           `json:"lookup_output"`
	MinStartCall            int           `json:"min_startcall"`
	MaxStartCall            int           `json:"max_startcall"`
	RejectFieldEmpty        int           `json:"field_empty"`
	RejectImsi              int           `json:"imsi"`
	RejectNotMo             int           `json:"not_mo"`
	RejectDurasi0           int           `json:"durasi_0"`
	RejectTapCode           int           `json:"tap_code"`
	RejectMOPartnerID       int           `json:"mo_partnerid"`
	TotalTapinPostpaid      int           `json:"total_tapin_postpaid"`
	TotalTapinPrepaid       int           `json:"total_tapin_prepaid"`
	RejectImsiDurasi        int           `json:"imsi_durasi"`
	RejectNotMoDurasi       int           `json:"not_mo_durasi"`
	RejectTapCodeDurasi     int           `json:"tap_code_durasi"`
	RejectMOPartnerIDDurasi int           `json:"mo_partnerid_durasi"`
}

// ListPartnerID :
type ListPartnerID struct {
	PartnerID string `json:"partner_id"`
	Status    int    `json:"status"`
}

// ProcessCheckdup :
type ProcessCheckdup struct {
	ProcessID      string `json:"process_id"`
	ProcesstypeID  string `json:"processtype_id"`
	BatchID        string `json:"batch_id"`
	DataTypeID     int    `json:"data_type_id"`
	Periode        string `json:"periode"`
	DayPeriode     string `json:"day"`
	MoMt           string `json:"mo_mt"`
	PostPre        string `json:"post_pre"`
	CdrID          int    `json:"cdr_id"`
	PathCdr        string `json:"path_cdr"`
	FilenameCdr    string `json:"filename_cdr"`
	DupID          int    `json:"dup_id"`
	PathDup        string `json:"path_dup"`
	FilenameDup    string `json:"filename_dup"`
	DBID           int    `json:"db_id"`
	PathDB         string `json:"path_db"`
	RejectID       int    `json:"reject_id"`
	PathReject     string `json:"path_reject"`
	FilenameReject string `json:"filename_reject"`
	UndupID        int    `json:"undup_id"`
	PathUndup      string `json:"path_undup"`
	FilenameUndup  string `json:"filename_undup"`
}

// StructCloseCheckDup :
type StructCloseCheckDup struct {
	StructProcess       ProcessCheckdup           `json:"process_conf"`
	ProcessStatusID     string                    `json:"processstatus_id"`
	TotalInput          int                       `json:"total_input"`
	TotalReject         int                       `json:"total_reject"`
	TotalDuplicate      int                       `json:"total_duplicate"`
	TotalUnduplicate    int                       `json:"total_unduplicate"`
	DuplicateDuration   int                       `json:"duplicate_duration"`
	UnduplicateDuration int                       `json:"unduplicate_duration"`
	MinStartcall        string                    `json:"min_startcall"`
	MaxStartcall        string                    `json:"max_startcall"`
	ErrorMessage        string                    `json:"error_message"`
	MapCount            map[string]map[string]int `json:"map_count"`
}

// ProcessRecon :
type ProcessRecon struct {
	ProcessID     string `json:"process_id"`
	ProcesstypeID string `json:"processtype_id"`
	BatchID       string `json:"batch_id"`
	DataTypeID    int    `json:"data_type_id"`
	Periode       string `json:"periode"`
	DayPeriode    string `json:"day"`
	MoMt          string `json:"mo_mt"`
	PostPre       string `json:"post_pre"`
	UndupID       int    `json:"id_undup"`
	PathUndup     string `json:"path_undup"`
	FilenameUndup string `json:"filename_undup"`
	OnlyID        int    `json:"only_id"`
	PathOnly      string `json:"path_only"`
	FilenameOnly  string `json:"filename_only"`
	MatchID       int    `json:"match_id"`
	PathMatch     string `json:"path_match"`
	FilenameMatch string `json:"filename_match"`
	MaxDelay      string `json:"max_delay"`
	MinStartcall  string `json:"min_startcall"`
	MaxStartcall  string `json:"max_startcall"`
}

// StructCloseRecon :
type StructCloseRecon struct {
	ProcessConf                ProcessRecon                        `json:"process_conf"`
	TotalInput                 int                                 `json:"total_input"`
	TotalMatch                 int                                 `json:"total_match"`
	TotalTapinOnly             int                                 `json:"total_tapin_only"`
	ProcessStatusID            int                                 `json:"process_status_id"`
	ErrorMessage               string                              `json:"error_message"`
	LogMatch                   map[int]map[string]int              `json:"log_match"`
	LogTapinOnly               map[string]*LogMatchContent         `json:"log_tapinonly"`
	LogMatchPartnerID          map[string]*LogMatchContent         `json:"log_match_partnerid"`
	LogMatchToleransiPartnerID map[string]map[int]*LogMatchContent `json:"log_match_toleransi_partnerid"`
}

// LogMatchContent :
type LogMatchContent struct {
	Durations int     `json:"durations"`
	Records   int     `json:"records"`
	Revenues  float64 `json:"revenues"`
}

// ProcessInsertDB :
type ProcessInsertDB struct {
	ProcessID     string `json:"process_id"`
	BatchID       string `json:"batch_id"`
	ProcessTypeID string `json:"processtype_id"`
	Periode       string `json:"periode"`
	DataTypeID    int    `json:"data_type_id"`
	DayPeriode    string `json:"day"`
	MoMt          string `json:"mo_mt"`
	PostPre       string `json:"post_pre"`
	InputID       int    `json:"id_input"`
	PathInput     string `json:"path_input"`
	FilenameInput string `json:"filename_input"`
}

// TaskInsertDB :
type TaskInsertDB struct {
	ProcessID     string `json:"process_id"`
	BatchID       string `json:"batch_id"`
	Periode       string `json:"periode"`
	DataType      string `json:"data_type"`
	PostPre       string `json:"post_pre"`
	MoMt          string `json:"mo_mt"`
	InputID       int    `json:"id_input"`
	FileType      string `json:"filetype"`
	PathInput     string `json:"path_input"`
	FilenameInput string `json:"filename_input"`
}

// DataOCS :
type DataOCS struct {
	BatchID       string `json:"batch_id"`
	Edtm          int    `json:"edtm"`
	Aparty        int    `json:"aparty"`
	Bparty        string `json:"bparty"`
	Imsi          int    `json:"imsi"`
	Callduration  int    `json:"callduration"`
	Roamingflag   int    `json:"roamingflag"`
	Camelroaming  int    `json:"camelroaming"`
	Accountdelta  string `json:"accountdelta"`
	Calltype      int    `json:"calltype"`
	ServiceFilter string `json:"service_filter"`
	TapCode       string `json:"tap_code"`
}

// DataOCSold :
type DataOCSold struct {
	BatchID                  string `json:"batch_id"`
	Edtm                     int    `json:"edtm"`
	Aparty                   int    `json:"aparty"`
	Locationnumbertypea      int    `json:"locationnumbertypea"`
	Locationnumbera          int    `json:"locationnumbera"`
	Bparty                   string `json:"bparty"`
	Locationnumbertypeb      int    `json:"locationnumbertypeb"`
	Locationnumberb          int    `json:"locationnumberb"`
	Nrofreroutings           int    `json:"nrofreroutings"`
	Imsi                     int    `json:"imsi"`
	Nrofnetworkreroutings    int    `json:"nrofnetworkreroutings"`
	Redirectingpartyid       int    `json:"redirectingpartyid"`
	Callduration1            int    `json:"callduration1"`
	Callduration             int    `json:"callduration"`
	Chargingid               int    `json:"chargingid"`
	Roamingflag              int    `json:"roamingflag"`
	Vlrnumber                int    `json:"vlrnumber"`
	Cellid                   int    `json:"cellid"`
	Negotiatedqos            string `json:"negotiatedqos"`
	Requestedqos             string `json:"requestedqos"`
	Subscribedqos            string `json:"subscribedqos"`
	Dialledapn               string `json:"dialledapn"`
	Eventtype                string `json:"eventtype"`
	Providerid               int    `json:"providerid"`
	Currency                 string `json:"currency"`
	Subscriberstatus         string `json:"subscriberstatus"`
	Forwardingflag           int    `json:"forwardingflag"`
	Firstcallflag            int    `json:"firstcallflag"`
	Camelroaming             int    `json:"camelroaming"`
	Timezone                 int    `json:"timezone"`
	Accountbalance           string `json:"accountbalance"`
	Accountdelta             string `json:"accountdelta"`
	Accountnumber            int    `json:"accountnumber"`
	Accountowner             int    `json:"accountowner"`
	Eventsource              int    `json:"eventsource"`
	Guidingresourcetype      string `json:"guidingresourcetype"`
	Roundedduration          int    `json:"roundedduration"`
	Totalvolume              int    `json:"totalvolume"`
	Roundedtotalvolume       int    `json:"roundedtotalvolume"`
	Eventstartperiod         int    `json:"eventstartperiod"`
	Chargeincludingallowance string `json:"chargeincludingallowance"`
	Discountamount           string `json:"discountamount"`
	Freetotalvolume          int    `json:"freetotalvolume"`
	Freeduration             int    `json:"freeduration"`
	Calldirection            string `json:"calldirection"`
	Chargecode               string `json:"chargecode"`
	Specialnumberind         int    `json:"specialnumberind"`
	Bonusinformation         string `json:"bonusinformation"`
	Internalcause            string `json:"internalcause"`
	Basiccause               string `json:"basiccause"`
	Timeband                 int    `json:"timeband"`
	Calltype                 int    `json:"calltype"`
	Bonusconsumed            string `json:"bonusconsumed"`
	VasCode                  string `json:"vas_code"`
	ServiceFilter            string `json:"service_filter"`
	NationalCallingZone      string `json:"national_calling_zone"`
	NationalCalledZone       string `json:"national_called_zone"`
	IntCallingZone           string `json:"int_calling_zone"`
	IntCalledZone            string `json:"int_called_zone"`
	Creditindicator          string `json:"creditindicator"`
	EventID                  string `json:"event_id"`
	Accesscode               string `json:"accesscode"`
	Countryname              string `json:"countryname"`
	Cpname                   string `json:"cpname"`
	Contentid                string `json:"contentid"`
	Ratinggroup              string `json:"ratinggroup"`
	Bftindicator             string `json:"bftindicator"`
	Unappliedamount          int    `json:"unappliedamount"`
	Partitionid              int    `json:"partitionid"`
	Rateddatetime            int    `json:"rateddatetime"`
	Area                     int    `json:"area"`
	Costband                 string `json:"costband"`
	Ratingofferid            int    `json:"ratingofferid"`
	Settlementindicator      int    `json:"settlementindicator"`
	TapCode                  string `json:"tap_code"`
	Mccmnc                   int    `json:"mccmnc"`
	Ratingpricingitemid      int    `json:"ratingpricingitemid"`
	Fileidentifier           int    `json:"fileidentifier"`
	Recordnumber             int    `json:"recordnumber"`
	Futurestring             string `json:"futurestring"`
}

// DataTAPIN : Struct of TAPIN Model
type DataTAPIN struct {
	BatchID         string `json:"batch_id"`
	CamelServiceKey int    `json:"camel_service_key"`
	Imsi            int    `json:"imsi"`
	Msisdn          int    `json:"msisdn"`
	Othertelnum     int    `json:"othertelnum"`
	Calleddigit     int    `json:"calleddigit"`
	Partnerid       string `json:"partnerid"`
	LocalTime       int    `json:"local_time"`
	Starttime       int    `json:"starttime"`
	Dealtime        int    `json:"dealtime"`
	Duration        int    `json:"duration"`
	CdrType         string `json:"cdr_type"`
	Servicename     string `json:"servicename"`
	TapFilename     string `json:"tap filename"`
	TapFee          string `json:"tap fee"`
	UtcTime         string `json:"utc_time"`
	FlagPostPre     int    `json:"flag_post_pre"`
	PlusUtcTime     int    `json:"plus_utc_time"`
}

// DataTAPINOnly : Struct of TAPIN Only Model
type DataTAPINOnly struct {
	BatchID              string `json:"batch_id"`
	Eventsource          string `json:"event_source"`
	Eventtypeid          string `json:"event_type_id"`
	Starttime            string `json:"starttime"`
	Reserve1             string `json:"reserve1"`
	Reserve2             string `json:"reserve2"`
	Reserve3             string `json:"reserve3"`
	Reserve4             string `json:"reserve4"`
	Reserve5             string `json:"reserve5"`
	Reserve6             string `json:"reserve6"`
	Reserve7             string `json:"reserve7"`
	Reserve8             string `json:"reserve8"`
	Reserve9             string `json:"reserve9"`
	Reserve10            string `json:"reserve10"`
	Callednumber         string `json:"called_number"`
	Operatorid1          string `json:"operator_id1"`
	Calltype             string `json:"call_type"`
	Charge               string `json:"charge"`
	Callingnumber        string `json:"calling_number"`
	Reserve11            string `json:"reserve11"`
	Tapdecimalplaces     string `json:"tapdecimalplaces"`
	Dialednumber         string `json:"dialed_number"`
	Duration             string `json:"duration"`
	Msisdn               string `json:"msisdn"`
	Sessionid            string `json:"session_id"`
	Callforwardindicator string `json:"call_forward_indicator"`
	Roamingtypeindicator string `json:"roaming_type_indicator"`
	Operatorid2          string `json:"operator_id2"`
	Localtimestamp       string `json:"localtimestamp"`
	Reserve12            string `json:"reserve12"`
	Reserve13            string `json:"reserve13"`
	Reserve14            string `json:"reserve14"`
	Reserve15            string `json:"reserve15"`
	Utctimeoffset        string `json:"utc_time_offset"`
	Reserve16            string `json:"reserve16"`
	Reserve17            string `json:"reserve17"`
	Reserve18            string `json:"reserve18"`
	Reserve19            string `json:"reserve19"`
	Tapfilename          string `json:"tapfilename"`
}

// StructCopyToTable :
type StructCopyToTable struct {
	Filename string `json:"filename"`
	Periode  string `json:"periode"`
}

// StructCloseInsertDB :
type StructCloseInsertDB struct {
	ProcessID       string `json:"process_id"`
	ProcessStatusID string `json:"process_status_id"`
	ErrorMessage    string `json:"error_message"`
}

// SlicedProcess :
type SlicedProcess struct {
	PathDB  string `json:"path_db"`
	Periode string `json:"periode"`
}

// DataStructure :
type DataStructure struct {
	TypeData map[string]DataStructureParameter `json:"data_type"`
}

// DataStructureParameter :
type DataStructureParameter struct {
	Parameter map[string]DataStructureProperties `json:"parameter"`
}

// DataStructureProperties :
type DataStructureProperties struct {
	Urutan     int `json:"urutan"`
	SortUrutan int `json:"sort_urutan"`
}

// DataType :
type DataType struct {
	DataTypeID   int    `json:"data_type_id"`
	DataTypeName string `json:"data_type"`
}

// CheckDupConf :
type CheckDupConf struct {
	Conf map[string]CheckDupParameter `json:"conf"`
}

// CheckDupParameter :
type CheckDupParameter struct {
	Type               string   `json:"type"`
	ParameterContains  string   `json:"parameter_contains"`
	ParameterCompare   []string `json:"parameter_compare"`
	ParameterToleransi []string `json:"parameter_toleransi"`
}

// LevelVolcomp :
type LevelVolcomp struct {
	Level     int                         `json:"level"`
	Conf      map[string]LevelVolcompConf `json:"conf"`
	Toleransi []ToleransiVolcomp          `json:"toleransi"`
}

// LevelVolcompConf :
type LevelVolcompConf struct {
	IDLevel            int      `json:"id_level"`
	Level              int      `json:"level"`
	Type               string   `json:"type"`
	ParameterCompare   []string `json:"parameter_compare"`
	ParameterToleransi []string `json:"parameter_toleransi"`
}

// ToleransiVolcomp :
type ToleransiVolcomp struct {
	IDToleransi int `json:"id_toleransi"`
	Level       int `json:"level"`
	Startcall   int `json:"startcall"`
	Durasi      int `json:"durasi"`
	Priorities  int `json:"priorities"`
	ActiveID    int `json:"active_id"`
}

// ProcessRegenerate :
type ProcessRegenerate struct {
	ProcessID         string `json:"process_id"`
	ProcesstypeID     string `json:"processtype_id"`
	BatchID           string `json:"batch_id"`
	DataTypeID        int    `json:"data_type_id"`
	Periode           string `json:"periode"`
	DayPeriode        string `json:"day"`
	MoMt              string `json:"mo_mt"`
	PostPre           string `json:"post_pre"`
	OnlyID            int    `json:"only_id"`
	PathOnly          string `json:"path_only"`
	FilenameOnly      string `json:"filename_only"`
	MatchID           int    `json:"match_id"`
	PathMatch         string `json:"path_match"`
	FilenameMatch     string `json:"filename_match"`
	MaxDelay          string `json:"max_delay"`
	TableName         string `json:"table_name"`
	OriTapinOnlyCount int    `json:"ori_tapin_only_count"`
}

// StructDataRegenerate :
type StructDataRegenerate struct {
	CountData      int      `json:"count_data"`
	DataRegenerate []string `json:"data_regenerate"`
}

// StructCloseRegenerate :
type StructCloseRegenerate struct {
	StructProcess     ProcessRegenerate `json:"process_conf"`
	ProcessStatusID   string            `json:"process_status_id"`
	TotalNewTapinOnly int               `json:"total_new_tapin_only"`
	ErrorMessage      string            `json:"error_message"`
}

// ProcessSendFile :
type ProcessSendFile struct {
	ProcessID  string `json:"process_id"`
	BatchID    string `json:"batch_id"`
	Periode    string `json:"periode"`
	DataTypeID int    `json:"data_type_id"`
	DayPeriode string `json:"day"`
	MoMt       string `json:"mo_mt"`
	PostPre    string `json:"post_pre"`
	FileTypeID int    `json:"filetype_id"`
	PathFile   string `json:"path_file"`
	Filename   string `json:"filename"`
}

// ReceiverSendFile :
type ReceiverSendFile struct {
	Host string `json:"host"`
	User string `json:"user"`
	Pass string `json:"pass"`
	Path string `json:"path"`
}

// StructCloseSendFile :
type StructCloseSendFile struct {
	ProcessID       string `json:"process_id"`
	BatchID         string `json:"batch_id"`
	Periode         string `json:"periode"`
	DayPeriode      string `json:"day"`
	DataTypeID      int    `json:"data_type_id"`
	ProcessStatusID string `json:"process_status_id"`
	ErrorMessage    string `json:"error_message"`
}

// ProcessDataOnly :
type ProcessDataOnly struct {
	ProcessID        string `json:"process_id"`
	BatchID          string `json:"batch_id"`
	Periode          string `json:"periode"`
	DataTypeID       int    `json:"data_type_id"`
	MoMt             string `json:"mo_mt"`
	PostPre          string `json:"post_pre"`
	FileTypeDB       int    `json:"filetype_id_db"`
	PathDB           string `json:"path_db"`
	MatchDB          string `json:"match_db"`
	FileTypeDataOnly int    `json:"filetype_id_data_only"`
	PathDataOnly     string `json:"path_data_only"`
	FilenameDataOnly string `json:"filename_data_only"`
}

// StructCloseDataOnly :
type StructCloseDataOnly struct {
	ProcessConf        ProcessDataOnly                 `json:"process_conf"`
	ProcessStatusID    int                             `json:"process_status_id"`
	ErrorMessage       string                          `json:"error_message"`
	CountInputRecord   int                             `json:"count_input_record"`
	CountInputDuration int                             `json:"count_input_duration"`
	CountOnlyRecord    int                             `json:"count_only_record"`
	CountOnlyDuration  int                             `json:"count_only_duration"`
	CountMatchRecord   int                             `json:"count_match_record"`
	CountMatchDuration int                             `json:"count_match_duration"`
	LogPartnerID       map[string]*LogMatchContentOnly `json:"log_partnerid"`
}

// LogMatchContentOnly :
type LogMatchContentOnly struct {
	DurationsInput int     `json:"durations_input"`
	RecordsInput   int     `json:"records_input"`
	RevenuesInput  float64 `json:"reveues_input"`
	DurationsMatch int     `json:"durations_match"`
	RecordsMatch   int     `json:"records_match"`
	RevenuesMatch  float64 `json:"reveues_match"`
	DurationsOnly  int     `json:"durations_only"`
	RecordsOnly    int     `json:"records_only"`
	RevenuesOnly   float64 `json:"reveues_only"`
}

// ProcessAlert :
type ProcessAlert struct {
	AlertID         int    `json:"alert_id"`
	PathFile        string `json:"path_file"`
	FilePattern     string `json:"file_pattern"`
	ReceiverEmail   string `json:"receiver_email"`
	SourcePath      string `json:"source_path"`
	MailConfID      int    `json:"mailconf_id"`
	MailConfTitle   string `json:"mailconf_title"`
	MailConfSubject string `json:"mailconf_subject"`
	MailConfMessage string `json:"mailconf_message"`
	AlertExec       string `json:"alert_exec"`
	LastAlert       string `json:"last_alert"`
}

// MailTask :
type MailTask struct {
	MailConfID         int    `json:"mailconf_id"`
	MailTaskTitle      string `json:"mailtask_title"`
	MailTaskSubject    string `json:"mailtask_subject"`
	MailTaskMessage    string `json:"mailtask_message"`
	MailTaskAttachment string `json:"mailtask_attachment"`
	MailtaskSendDate   string `json:"mailtask_send_date"`
	ProcessID          string `json:"process_id"`
}

// MailTaskProcess :
type MailTaskProcess struct {
	MailTaskID         int    `json:"mailtask_id"`
	MailConfID         int    `json:"mailconf_id"`
	MailTaskTittle     string `json:"mailtask_title"`
	MailTaskSubject    string `json:"mailtask_subject"`
	MailTaskMessage    string `json:"mailtask_message"`
	MailTaskAttachment string `json:"mailtask_attachment"`
	MailTaskSendDate   string `json:"mailtask_send_date"`
	MailTaskStatus     int    `json:"mailtask_status"`
	ProcessID          string `json:"process_id"`
	ReceiverEmail      string `json:"receiver_email"`
}

// AlertSMTP :
type AlertSMTP struct {
	Key map[string]string `json:"key"`
}

// ProcessReportBck :
type ProcessReportBck struct {
	ProcessID         string `json:"process_id"`
	BatchID           string `json:"batch_id"`
	Periode           string `json:"periode"`
	DataTypeID        string `json:"data_type_id"`
	MoMt              string `json:"mo_mt"`
	PostPre           string `json:"post_pre"`
	FilenameSource    string `json:"filename_source"`
	FilenamePostPre   string `json:"filename_post_pre"`
	ReportID          int    `json:"report_id"`
	ReportPath        string `json:"report_path"`
	ReportFilename    string `json:"report_filename"`
	SourceInput       int    `json:"source_input"`
	SplitMO           int    `json:"split_mo"`
	SplitMT           int    `json:"split_mt"`
	SplitReject       int    `json:"split_reject"`
	LookupInput       int    `json:"lookup_input"`
	LookupReject      int    `json:"lookup_reject"`
	LookupOutput      int    `json:"lookup_output"`
	CekdupInput       int    `json:"cekdup_input"`
	CekdupReject      int    `json:"cekdup_reject"`
	CekdupDuplicate   int    `json:"cekdup_duplicate"`
	CekdupUnduplicate int    `json:"cekdup_unduplicate"`
	VolcompInput      int    `json:"volcomp_input"`
	VolcompMatch      int    `json:"volcomp_match"`
	VolcompTapinOnly  int    `json:"volcomp_tapin_only"`
	FileTapinOnly     string `json:"file_tapin_only"`
	RejectFieldEmpty  int    `json:"field_empty"`
	RejectImsi        int    `json:"imsi"`
	RejectNotMo       int    `json:"not_mo"`
	RejectDurasi0     int    `json:"durasi_0"`
	RejectTapCode     int    `json:"tap_code"`
}

// ProcessReport :
type ProcessReport struct {
	ProcessID         string `json:"process_id"`
	BatchID           string `json:"batch_id"`
	Periode           string `json:"periode"`
	Day               string `json:"day"`
	StartDate         string `json:"startdate"`
	DataTypeID        string `json:"data_type_id"`
	MoMt              string `json:"mo_mt"`
	PostPre           string `json:"post_pre"`
	FilenameSource    string `json:"filename_source"`
	ReportID          int    `json:"report_id"`
	ReportPath        string `json:"report_path"`
	ReportFilename    string `json:"report_filename"`
	SourceInput       int    `json:"source_input"`
	SplitMO           int    `json:"split_mo"`
	SplitMT           int    `json:"split_mt"`
	SplitReject       int    `json:"split_reject"`
	SplitFilename     string `json:"split_filename"`
	LookupInput       int    `json:"lookup_input"`
	LookupReject      int    `json:"lookup_reject"`
	LookupOutput      int    `json:"lookup_output"`
	LookupFilename    string `json:"lookup_filename"`
	CekdupInput       int    `json:"cekdup_input"`
	CekdupReject      int    `json:"cekdup_reject"`
	CekdupDuplicate   int    `json:"cekdup_duplicate"`
	CekdupUnduplicate int    `json:"cekdup_unduplicate"`
	CekdupFilename    string `json:"cekdup_filename"`
	VolcompInput      int    `json:"volcomp_input"`
	VolcompMatch      int    `json:"volcomp_match"`
	VolcompTapinOnly  int    `json:"volcomp_tapin_only"`
	FileTapinOnly     string `json:"file_tapin_only"`
	RejectFieldEmpty  int    `json:"field_empty"`
	RejectImsi        int    `json:"imsi"`
	RejectMOPartnerID int    `json:"mo_partner_id"`
	RejectDurasi0     int    `json:"durasi_0"`
	RejectTapCode     int    `json:"tap_code"`
	LookupRejectFile  string `json:"lookup_reject_file"`
	FilterTapCode     string `json:"filter_tapcode"`
}

// StructCloseReport :
type StructCloseReport struct {
	ProcessConf     ProcessReport `json:"process_conf"`
	ProcessStatusID int           `json:"process_status_id"`
	ErrorMessage    string        `json:"error_message"`
}

// StructLogMatch :
type StructLogMatch struct {
	ProcessID   string `json:"process_id"`
	BatchID     string `json:"batch_id"`
	IDToleransi int    `json:"id_toleransi"`
	Startcall   int    `json:"startcall"`
	Durasi      int    `json:"durasi"`
	TotalMatch  int    `json:"total_match"`
}

// StructLogTapinOnly :
type StructLogTapinOnly struct {
	ProcessID     string `json:"process_id"`
	BatchID       string `json:"batch_id"`
	PartnerID     string `json:"partner_id"`
	TotalRecord   int    `json:"total_record"`
	TotalDuration int    `json:"total_duration"`
}

// ProcessReconTapinOnly :
type ProcessReconTapinOnly struct {
	ProcessID              string `json:"process_id"`
	BatchID                string `json:"batch_id"`
	Periode                string `json:"periode"`
	DataTypeID             int    `json:"data_type_id"`
	MoMt                   string `json:"mo_mt"`
	PostPre                string `json:"post_pre"`
	MatchTapinOnlyID       int    `json:"match_tapin_only_id"`
	MatchTapinOnlyPath     string `json:"match_tapin_only_path"`
	MatchTapinOnlyFilename string `json:"match_tapin_only_filename"`
	OnlyTapinOnlyID        int    `json:"only_tapin_only_id"`
	OnlyTapinOnlyPath      string `json:"only_tapin_only_path"`
	OnlyTapinOnlyFilename  string `json:"only_tapin_only_filename"`
	MinStartcall           string `json:"min_startcall"`
	MaxStartcall           string `json:"max_startcall"`
}

// ListTapinOnly :
type ListTapinOnly struct {
	FileID      int    `json:"file_id"`
	Periode     string `json:"periode"`
	BatchIDAsal string `json:"batch_id_asal"`
	FileTypeID  int    `json:"file_type_id"`
	PathFile    string `json:"pathfile"`
	Filename    string `json:"filename"`
	MoMt        string `json:"mo_mt"`
	PostPre     string `json:"post_pre"`
}

// ProcessReportMonthly :
type ProcessReportMonthly struct {
	ProcessID      string `json:"process_id"`
	BatchID        string `json:"batch_id"`
	Periode        string `json:"periode"`
	MoMt           string `json:"mo_mt"`
	PostPre        string `json:"post_pre"`
	ReportID       int    `json:"report_id"`
	ReportPath     string `json:"report_path"`
	ReportFilename string `json:"report_filename"`
	FilterTapCode  string `json:"filter_tapcode"`
}

// StructLogReconMonthly :
type StructLogReconMonthly struct {
	Periode           null.String `json:"periode"`
	MoMt              null.String `json:"mo_mt"`
	PostPre           null.String `json:"post_pre"`
	Input             null.Int    `json:"input"`
	Match             null.Int    `json:"match"`
	TapinOnly         null.Int    `json:"tapin_only"`
	TapinOnlyDuration null.Int    `json:"tapin_only_duration"`
	MatchDuration     null.Int    `json:"match_duration"`
	SplitInput        null.Int    `json:"split_input"`
	SplitMo           null.Int    `json:"split_mo"`
	SplitMt           null.Int    `json:"split_mt"`
	SplitReject       null.Int    `json:"split_reject"`
	Duplicate         null.Int    `json:"duplicate"`
	LookupReject      null.Int    `json:"lookup_reject"`
}

// StructLogCekdup :
type StructLogCekdup struct {
	Periode             null.String `json:"periode"`
	DataType            null.String `json:"data_type"`
	MoMt                null.String `json:"mo_mt"`
	PostPre             null.String `json:"post_pre"`
	Input               null.Int    `json:"input"`
	Duplicate           null.Int    `json:"duplicate"`
	Unduplicate         null.Int    `json:"unduplicate"`
	DuplicateDuration   null.Int    `json:"duplicate_duration"`
	UnduplicateDuration null.Int    `json:"unduplicate_duration"`
}

// StructLogOCSOnly :
type StructLogOCSOnly struct {
	Periode       null.String `json:"periode"`
	MoMt          null.String `json:"mo_mt"`
	PostPre       null.String `json:"post_pre"`
	InputRecord   null.Int    `json:"input_record"`
	InputDuration null.Int    `json:"input_duration"`
	OnlyRecord    null.Int    `json:"only_record"`
	OnlyDuration  null.Int    `json:"only_duration"`
	MatchRecord   null.Int    `json:"match_record"`
	MatchDuration null.Int    `json:"match_duration"`
	SplitInput    null.Int    `json:"split_input"`
	SplitMo       null.Int    `json:"split_mo"`
	SplitMt       null.Int    `json:"split_mt"`
	SplitReject   null.Int    `json:"split_reject"`
	Duplicate     null.Int    `json:"duplicate"`
	LookupReject  null.Int    `json:"lookup_reject"`
}

// StructLogRecon :
type StructLogRecon struct {
	Periode            null.String `json:"periode"`
	PostPre            null.String `json:"post_pre"`
	MoMt               null.String `json:"mo_mt"`
	PartnerID          null.String `json:"partner_id"`
	TapinInputRecord   null.Int    `json:"tapin_input_record"`
	TapinInputDuration null.Int    `json:"tapin_input_duration"`
	TapinOnlyRecord    null.Int    `json:"tapin_only_record"`
	TapinOnlyDuration  null.Int    `json:"tapin_only_duration"`
	TapinOnlyRevenue   null.Float  `json:"tapin_only_revenue"`
	TapinMatchRecord   null.Int    `json:"tapin_match_record"`
	TapinMatchDuration null.Int    `json:"tapin_match_duration"`
	TapinMatchRevenue  null.Float  `json:"tapin_match_revenue"`
	OcsInputRecord     null.Int    `json:"ocs_input_record"`
	OcsInputDuration   null.Int    `json:"ocs_input_duration"`
	OcsOnlyRecord      null.Int    `json:"ocs_only_record"`
	OcsOnlyDuration    null.Int    `json:"ocs_only_duration"`
	OcsMatchRecord     null.Int    `json:"ocs_match_record"`
	OcsMatchDuration   null.Int    `json:"ocs_match_duration"`
	OcsOnlyRevenue     null.Float  `json:"ocs_only_revenue"`
	OcsMatchRevenue    null.Float  `json:"ocs_match_revenue"`
	OcsInputRevenue    null.Float  `json:"ocs_input_revenue"`
}

// StructLogMatchToleransiGroup :
type StructLogMatchToleransiGroup struct {
	Periode           null.String `json:"periode"`
	PostPre           null.String `json:"post_pre"`
	MoMt              null.String `json:"mo_mt"`
	PartnerID         null.String `json:"partner_id"`
	Record0           null.Int    `json:"record_0"`
	Record30          null.Int    `json:"record_30"`
	Record60          null.Int    `json:"record_60"`
	Record120         null.Int    `json:"record_120"`
	RecordBigger120   null.Int    `json:"record_bigger_120"`
	Duration0         null.Int    `json:"duration_0"`
	Duration30        null.Int    `json:"duration_30"`
	Duration60        null.Int    `json:"duration_60"`
	Duration120       null.Int    `json:"duration_120"`
	DurationBigger120 null.Int    `json:"duration_bigger_120"`
}

// StructLogReportMonthly :
type StructLogReportMonthly struct {
	BatchID                  null.String `json:"batch_id"`
	Periode                  null.String `json:"periode"`
	Day                      null.String `json:"day"`
	DataTypeID               null.String `json:"data_type_id"`
	MoMt                     null.String `json:"mo_mt"`
	PostPre                  null.String `json:"post_pre"`
	FilenameCDR              null.String `json:"filename_cdr"`
	SplitInput               null.Int    `json:"split_input"`
	SplitReject              null.Int    `json:"split_reject"`
	SplitMO                  null.Int    `json:"split_mo"`
	SplitMT                  null.Int    `json:"split_mt"`
	SplitFilename            null.String `json:"split_filename"`
	LookupInput              null.Int    `json:"lookup_input"`
	LookupReject             null.Int    `json:"lookup_reject"`
	LookupOutput             null.Int    `json:"lookup_output"`
	LookupFilename           null.String `json:"lookup_filename"`
	CekdupInput              null.Int    `json:"cekdup_input"`
	CekdupDuplicate          null.Int    `json:"cekdup_duplicate"`
	CekdupUnduplicate        null.Int    `json:"cekdup_unduplicate"`
	CekdupFilename           null.String `json:"cekdup_filename"`
	VolcompInput             null.Int    `json:"volcomp_input"`
	VolcompMatch             null.Int    `json:"volcomp_match"`
	VolcompTapinOnly         null.Int    `json:"volcomp_tapin_only"`
	VolcompTapinOnlyDuration null.Int    `json:"volcomp_tapin_only_duration"`
	FileTapinOnly            null.String `json:"file_tapin_only"`
	RejectFieldEmpty         null.Int    `json:"field_empty"`
	RejectImsi               null.Int    `json:"imsi"`
	RejectDurasi0            null.Int    `json:"durasi_0"`
	RejectTapCode            null.Int    `json:"tap_code"`
	RejectMOPartnerID        null.Int    `json:"mo_partner_id"`
	RejectImsiDurasi         null.Int    `json:"imsi_durasi"`
	RejectTapCodeDurasi      null.Int    `json:"tap_code_durasi"`
	RejectMOPartnerIDDurasi  null.Int    `json:"mo_partner_id_durasi"`
	LookupRejectFilename     null.String `json:"lookup_reject_filename"`
}

// StructLogCekdupPartnerID :
type StructLogCekdupPartnerID struct {
	Periode             null.String `json:"periode"`
	DataType            null.String `json:"data_type"`
	MoMt                null.String `json:"mo_mt"`
	PostPre             null.String `json:"post_pre"`
	PartnerID           null.String `json:"partner_id"`
	Input               null.Int    `json:"input"`
	Duplicate           null.Int    `json:"duplicate"`
	Unduplicate         null.Int    `json:"unduplicate"`
	DuplicateDuration   null.Int    `json:"duplicate_duration"`
	UnduplicateDuration null.Int    `json:"unduplicate_duration"`
}

// StructLogCekdupTopTen :
type StructLogCekdupTopTen struct {
	Periode     null.String `json:"periode"`
	PostPre     null.String `json:"post_pre"`
	MoMt        null.String `json:"mo_mt"`
	Imsi        null.String `json:"imsi"`
	BNumber     null.String `json:"bnumber"`
	Starttime   null.String `json:"starttime"`
	PartnerID   null.String `json:"partner_id"`
	RecordCount null.Int    `json:"record_count"`
	SumDuration null.Int    `json:"sum_duration"`
}

// StructCloseReportMonthly :
type StructCloseReportMonthly struct {
	ProcessConf     *ProcessReportMonthly `json:"process_conf"`
	ProcessStatusID int                   `json:"process_status_id"`
	ErrorMessage    string                `json:"error_message"`
}

// ProcessGenerateL3 :
type ProcessGenerateL3 struct {
	ProcessID         string `json:"process_id"`
	BatchID           string `json:"batch_id"`
	Periode           string `json:"periode"`
	PostPre           string `json:"post_pre"`
	MoMt              string `json:"mo_mt"`
	FileTypeMatch     int    `json:"filetype_match"`
	PathMatch         string `json:"path_match"`
	FileMatch         string `json:"file_match"`
	FileTypeOCSOnly   int    `json:"filetype_ocs_only"`
	PathOCSOnly       string `json:"path_ocs_only"`
	FileOCSOnly       string `json:"file_ocs_only"`
	FileTypeTAPINOnly int    `json:"filetype_tapin_only"`
	PathTAPINOnly     string `json:"path_tapin_only"`
	FileTAPINOnly     string `json:"file_tapin_only"`
	FileTypeOCSDup    int    `json:"filetype_ocs_dup"`
	PathOCSDup        string `json:"path_ocs_dup"`
	FileOCSDup        string `json:"file_ocs_dup"`
	FileTypeTAPINDup  int    `json:"filetype_tapin_dup"`
	PathTAPINDup      string `json:"path_tapin_dup"`
	FileTAPINDup      string `json:"file_tapin_dup"`
}

// StructDataL3 :
type StructDataL3 struct {
	PartnerID null.String `json:"partner_id"`
	StartTime null.String `json:"start_time"`
	Anum      null.String `json:"a_num"`
	Bnum      null.String `json:"b_num"`
	Imsi      null.String `json:"imsi"`
	Duration  null.Int    `json:"duration"`
	Charge    null.String `json:"charge"`
}

// StructDataL3Match :
type StructDataL3Match struct {
	PartnerID      null.String `json:"partner_id"`
	StartTimeTapin null.String `json:"start_time_tapin"`
	AnumTapin      null.String `json:"a_num_tapin"`
	BnumTapin      null.String `json:"b_num_tapin"`
	ImsiTapin      null.String `json:"imsi_tapin"`
	DurationTapin  null.Int    `json:"duration_tapin"`
	ChargeTapin    null.String `json:"charge_tapin"`
	StartTimeOCS   null.String `json:"start_time_ocs"`
	AnumOCS        null.String `json:"a_num_ocs"`
	BnumOCS        null.String `json:"b_num_ocs"`
	ImsiOCS        null.String `json:"imsi_ocs"`
	DurationOCS    null.Int    `json:"duration_ocs"`
	ChargeOCS      null.String `json:"charge_ocs"`
	TimeDiff       null.Float  `json:"time_diff"`
	DurationDiff   null.Int    `json:"duration_diff"`
	ChargeDiff     null.Float  `json:"charge_diff"`
}

// StructCloseGenerateL3 :
type StructCloseGenerateL3 struct {
	ProcessConf     *ProcessGenerateL3 `json:"process_conf"`
	ProcessStatusID int                `json:"process_status_id"`
	ErrorMessage    string             `json:"error_message"`
}
