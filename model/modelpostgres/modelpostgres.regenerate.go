package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// RegenerateModel :
type RegenerateModel struct{}

// GetRegenerateProcess : Function to Get Process Regenerate
func (p *RegenerateModel) GetRegenerateProcess() ([]ProcessRegenerate, error) {
	db := app.Appl.PostgreDB

	var listProcess []ProcessRegenerate

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_regenerate;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessRegenerate{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.ProcesstypeID, &scanProcess.BatchID, &scanProcess.DataTypeID, &scanProcess.Periode, &scanProcess.DayPeriode,
			&scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.OnlyID, &scanProcess.PathOnly, &scanProcess.FilenameOnly, &scanProcess.MatchID,
			&scanProcess.PathMatch, &scanProcess.FilenameMatch, &scanProcess.MaxDelay, &scanProcess.TableName, &scanProcess.OriTapinOnlyCount,
		)

		listProcess = append(listProcess, scanProcess)
	}

	return listProcess, nil
}

// GetRegenerateData : Function to Get Data Regenerate
func (p *RegenerateModel) GetRegenerateData(tablename, batchID string) (StructDataRegenerate, error) {
	db := app.Appl.PostgreDB

	var dataRegenerate StructDataRegenerate

	// perform a db.Query
	countData := db.QueryRow("SELECT count(*) FROM roaming_vc_schema."+tablename+" WHERE batch_id = $1 AND to_file = 1;", batchID)
	countData.Scan(&dataRegenerate.CountData)

	// perform a db.Query
	rows, err := db.Query("SELECT concat_ws(',', event_source, event_type_id, starttime, reserve1, reserve2, reserve3, "+
		"reserve4, reserve5, reserve6, reserve7, reserve8, reserve9, reserve10, called_number, operator_id1, call_type, "+
		"charge, calling_number, reserve11, tapdecimalplaces, dialed_number, duration, msisdn, session_id, call_forward_indicator, "+
		"roaming_type_indicator, operator_id2, localtimestamp, reserve12, reserve13, reserve14, reserve15, utc_time_offset, reserve16, "+
		"reserve17, reserve18, reserve19, tapfilename) FROM roaming_vc_schema."+tablename+" WHERE batch_id = $1 AND to_file = 1;", batchID)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return StructDataRegenerate{}, err
	}

	for rows.Next() {
		var rowTapin string
		rows.Scan(&rowTapin)

		dataRegenerate.DataRegenerate = append(dataRegenerate.DataRegenerate, rowTapin)
	}

	return dataRegenerate, nil
}

// CloseRegenerateProcess : Function to Close Regenerate Proccess
func (p *RegenerateModel) CloseRegenerateProcess(param StructCloseRegenerate) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert

	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_regenerate($1,$2,$3,$4,$5,$6,$7,$8);",
		param.StructProcess.ProcessID, param.StructProcess.BatchID, param.StructProcess.Periode, param.StructProcess.DataTypeID,
		param.StructProcess.DayPeriode, param.ProcessStatusID, param.TotalNewTapinOnly, param.ErrorMessage)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
