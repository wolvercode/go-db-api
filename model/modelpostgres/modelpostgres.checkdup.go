package modelpostgres

import (
	"fmt"
	"strings"

	"bitbucket.org/billing/go-db-api/app"
)

// CheckDupModel :
type CheckDupModel struct{}

// GetProcessCheckDup : Function to Get Process CheckDup
func (p *CheckDupModel) GetProcessCheckDup() ([]ProcessCheckdup, error) {
	db := app.Appl.PostgreDB

	var processCheckdup []ProcessCheckdup

	// perform a db.Query
	// rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_getcheck_dup;")
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_checkdup;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessCheckdup{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.ProcesstypeID, &scanProcess.BatchID, &scanProcess.DataTypeID,
			&scanProcess.Periode, &scanProcess.DayPeriode, &scanProcess.MoMt, &scanProcess.PostPre, &scanProcess.CdrID, &scanProcess.PathCdr,
			&scanProcess.FilenameCdr, &scanProcess.DupID, &scanProcess.PathDup, &scanProcess.FilenameDup, &scanProcess.DBID, &scanProcess.PathDB,
			&scanProcess.RejectID, &scanProcess.PathReject, &scanProcess.FilenameReject, &scanProcess.UndupID, &scanProcess.PathUndup, &scanProcess.FilenameUndup,
		)

		processCheckdup = append(processCheckdup, scanProcess)
	}

	return processCheckdup, nil
}

// CloseProcessCheckDup : Function to Close CheckDup Proccess
func (p *CheckDupModel) CloseProcessCheckDup(param StructCloseCheckDup) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_checkdup($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27);",
		param.StructProcess.ProcessID, param.StructProcess.BatchID, param.StructProcess.Periode, param.StructProcess.DataTypeID,
		param.StructProcess.PathReject, param.StructProcess.RejectID, param.StructProcess.FilenameReject, param.StructProcess.PathDup,
		param.StructProcess.DupID, param.StructProcess.FilenameDup, param.StructProcess.PathDB, param.StructProcess.DBID, "",
		param.StructProcess.PathUndup, param.StructProcess.UndupID, param.StructProcess.FilenameUndup, param.TotalInput, param.TotalReject,
		param.TotalDuplicate, param.TotalUnduplicate, param.MinStartcall, param.MaxStartcall, param.StructProcess.DayPeriode,
		param.ProcessStatusID, param.ErrorMessage, param.UnduplicateDuration, param.DuplicateDuration)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	// Inserting Log Match PartnerID
	if len(param.MapCount) > 0 {
		query := "INSERT INTO roaming_vc_schema.vc_l_cekdup_partnerid (process_id,partner_id,record_duplicate,record_unduplicate,duration_duplicate,duration_unduplicate) VALUES"
		for partnerID := range param.MapCount {
			query = fmt.Sprintf("%s ('%s','%s',%d,%d,%d,%d),", query, param.StructProcess.ProcessID, partnerID,
				param.MapCount[partnerID]["dup_records"], param.MapCount[partnerID]["undup_records"], param.MapCount[partnerID]["dup_dur"], param.MapCount[partnerID]["undup_dur"])
		}

		query = query[0:len(query)-1] + ";"

		statement, err = db.Query(query)
		// if there is an error inserting, handle it
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		statement.Close()
	}

	return nil
}

// GetCheckDupConf : Function to Get CheckDup Configuration
func (p *CheckDupModel) GetCheckDupConf() (CheckDupConf, error) {
	db := app.Appl.PostgreDB

	var checkDupConf CheckDupConf

	checkDupParameter := map[string]CheckDupParameter{}
	query := "select a.type, a.parameter_compare, a.parameter_toleransi, " +
		"a.parameter_contains " +
		"from roaming_vc_schema.vc_t_conf_checkdup a " +
		"ORDER BY a.id_checkdup ASC;"
	rowsConf, err := db.Query(query)
	if err != nil {
		fmt.Println(err.Error())
		return CheckDupConf{}, err
	}

	for rowsConf.Next() {
		scanProcess := CheckDupParameter{}
		var scanParameterCompare string
		var arParameterCompare []string

		var scanParameterToleransi string
		var arParameterToleransi []string
		rowsConf.Scan(&scanProcess.Type, &scanParameterCompare, &scanParameterToleransi, &scanProcess.ParameterContains)

		columns := strings.Split(scanParameterCompare, "|")
		for _, v := range columns {
			var urutan string
			err := db.QueryRow("SELECT urutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 AND fieldname = $2", scanProcess.Type, v).Scan(&urutan)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			arParameterCompare = append(arParameterCompare, urutan)
		}

		columns = strings.Split(scanParameterToleransi, "|")
		for _, v := range columns {
			var urutan string
			err := db.QueryRow("SELECT urutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 AND fieldname = $2", scanProcess.Type, v).Scan(&urutan)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			arParameterToleransi = append(arParameterToleransi, urutan)
		}

		scanProcess.ParameterCompare = arParameterCompare
		scanProcess.ParameterToleransi = arParameterToleransi

		checkDupParameter[scanProcess.Type] = scanProcess

		checkDupConf.Conf = checkDupParameter
	}

	return checkDupConf, nil
}
