package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// LookupModel :
type LookupModel struct{}

// LastLookup :
type LastLookup struct {
	PathImsi    string `json:"path_imsi"`
	PathTapCode string `json:"path_tapcode"`
	PathAnum    string `json:"path_anum"`
	Lastlookup  string `json:"last_lookup"`
}

// GetProcessSplit : Function to Get Process Split
func (p *LookupModel) GetProcessSplit() ([]ProcessSplit, error) {
	db := app.Appl.PostgreDB

	var outputProcess []ProcessSplit

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_split;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessSplit{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.ProcesstypeID, &scanProcess.BatchID, &scanProcess.DataTypeID,
			&scanProcess.Periode, &scanProcess.DayPeriode, &scanProcess.FlagImsi, &scanProcess.CdrID, &scanProcess.PathCdr, &scanProcess.FilenameCdr,
			&scanProcess.OutID, &scanProcess.MoPath, &scanProcess.MoFilename, &scanProcess.MtPath, &scanProcess.MtFilename,
			&scanProcess.RejectID, &scanProcess.RejectPath, &scanProcess.RejectFilename,
		)

		outputProcess = append(outputProcess, scanProcess)
	}

	return outputProcess, nil
}

// GetListPartnerID :
func (p *LookupModel) GetListPartnerID() ([]ListPartnerID, error) {
	db := app.Appl.PostgreDB

	var output []ListPartnerID

	rows, err := db.Query("SELECT * FROM roaming_vc_schema.vc_t_partnerid WHERE status = 1")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ListPartnerID{}
		rows.Scan(&scanProcess.PartnerID, &scanProcess.Status)
		output = append(output, scanProcess)
	}

	return output, nil
}

// CloseProcessSplit : Function to Close Split Proccess
func (p *LookupModel) CloseProcessSplit(param ClosingSplit) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_split($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);",
		param.StructProcess.ProcessID, param.StructProcess.BatchID, param.StructProcess.Periode, param.StructProcess.DayPeriode,
		param.StructProcess.DataTypeID, param.ProcessStatusID, param.StructProcess.FlagImsi, param.ErrorMessage,
		param.TotalInput, param.TotalMo, param.TotalMt, param.TotalReject,
		param.StructProcess.OutID, param.StructProcess.MoPath, param.StructProcess.MoFilename,
		param.StructProcess.MtPath, param.StructProcess.MtFilename,
		param.StructProcess.RejectID, param.StructProcess.RejectPath, param.StructProcess.RejectFilename,
	)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// GetProcessLookup : Function to Get Process Lookup
func (p *LookupModel) GetProcessLookup() ([]ProcessLookup, error) {
	db := app.Appl.PostgreDB

	var outputProcess []ProcessLookup

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_lookup;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessLookup{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.ProcesstypeID, &scanProcess.BatchID, &scanProcess.DataTypeID,
			&scanProcess.Periode, &scanProcess.DayPeriode, &scanProcess.FlagImsi, &scanProcess.MoMt,
			&scanProcess.CdrID, &scanProcess.PathCdr, &scanProcess.FilenameCdr,
			&scanProcess.DBImsiID, &scanProcess.DBImsiPath, &scanProcess.DBTapCodeID, &scanProcess.DBTapCodePath,
			&scanProcess.OutID, &scanProcess.OutPath, &scanProcess.OutFilename,
			&scanProcess.OutPathPost, &scanProcess.OutFilenamePost,
			&scanProcess.OutPathPre, &scanProcess.OutFilenamePre,
			&scanProcess.RejectID, &scanProcess.RejectPath, &scanProcess.RejectFilename,
			&scanProcess.DBAnumID, &scanProcess.DBAnumPath,
		)

		outputProcess = append(outputProcess, scanProcess)
	}

	return outputProcess, nil
}

// GetLastLookup :
func (p *LookupModel) GetLastLookup() (LastLookup, error) {
	db := app.Appl.PostgreDB

	var outputProcess LastLookup

	// perform a db.Query
	rows, err := db.Query("SELECT b.pathfile, c.pathfile, d.pathfile, a.valuesysconf " +
		"FROM " +
		"roaming_vc_schema.vc_p_sysconf a " +
		"LEFT JOIN roaming_vc_schema.vc_p_filetype b ON b.filetype = 'DB_IMSI' " +
		"LEFT JOIN roaming_vc_schema.vc_p_filetype c ON c.filetype = 'DB_TAPCODE' " +
		"left join roaming_vc_schema.vc_p_filetype d on d.filetype = 'DB_ANUM'" +
		"WHERE a.sysconf = 'LAST_LOOKUP';")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return LastLookup{}, err
	}

	for rows.Next() {
		rows.Scan(&outputProcess.PathImsi, &outputProcess.PathTapCode, &outputProcess.PathAnum, &outputProcess.Lastlookup)
	}

	return outputProcess, nil
}

// UpdateLookup : Function to Update Lookup
func (p *ProcessModel) UpdateLookup(currentLookup string) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("Update roaming_vc_schema.vc_p_sysconf Set valuesysconf = $1 Where sysconf = 'LAST_LOOKUP';", currentLookup)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}

// CloseProcessLookup : Function to Close Lookup Proccess
func (p *LookupModel) CloseProcessLookup(param StructCloseLookup) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_lookup($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35);",
		param.StructProcess.ProcessID, param.StructProcess.BatchID, param.StructProcess.Periode, param.StructProcess.DayPeriode,
		param.StructProcess.DataTypeID, param.ProcessStatusID, param.StructProcess.FlagImsi, param.ErrorMessage,
		param.LookupInput, param.LookupReject, param.LookupOutput, param.MinStartCall, param.MaxStartCall,
		param.StructProcess.OutID, param.StructProcess.OutPath, param.StructProcess.OutFilename,
		param.StructProcess.RejectID, param.StructProcess.RejectPath, param.StructProcess.RejectFilename,
		param.RejectFieldEmpty, param.RejectImsi, param.RejectNotMo, param.RejectDurasi0, param.RejectTapCode, param.RejectMOPartnerID,
		param.StructProcess.OutPathPost, param.StructProcess.OutFilenamePost, param.TotalTapinPostpaid,
		param.StructProcess.OutPathPre, param.StructProcess.OutFilenamePre, param.TotalTapinPrepaid,
		param.RejectImsiDurasi, param.RejectNotMoDurasi, param.RejectTapCodeDurasi, param.RejectMOPartnerIDDurasi,
	)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
