package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// TapCodeModel :
type TapCodeModel struct{}

// StructTapCode :
type StructTapCode struct {
	ID      int    `json:"id"`
	TapCode string `json:"tap_code"`
	CellID  int    `json:"cell_id"`
}

// GetTapCode : Function to Get Tap Code
func (p *TapCodeModel) GetTapCode() ([]StructTapCode, error) {
	db := app.Appl.PostgreDB

	var output []StructTapCode

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.vc_t_tapcode;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructTapCode{}
		rows.Scan(
			&scanProcess.ID, &scanProcess.TapCode, &scanProcess.CellID,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}
