package modelpostgres

import (
	"fmt"
	"strings"

	"bitbucket.org/billing/go-db-api/app"
)

// ReconTapinOnlyModel :
type ReconTapinOnlyModel struct{}

// GetProcessReconTapinOnly : Function to Get Process Recon
func (p *ReconTapinOnlyModel) GetProcessReconTapinOnly() ([]ProcessReconTapinOnly, error) {
	db := app.Appl.PostgreDB

	var output []ProcessReconTapinOnly

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_rekon_tapinonly;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessReconTapinOnly{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.DataTypeID, &scanProcess.MoMt, &scanProcess.PostPre,
			&scanProcess.MatchTapinOnlyID, &scanProcess.MatchTapinOnlyPath, &scanProcess.MatchTapinOnlyFilename,
			&scanProcess.OnlyTapinOnlyID, &scanProcess.OnlyTapinOnlyPath, &scanProcess.OnlyTapinOnlyFilename,
			&scanProcess.MinStartcall, &scanProcess.MaxStartcall,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// GetListTapinOnly : Function to Get Process Recon
func (p *ReconTapinOnlyModel) GetListTapinOnly(periode, moMt, postPre string) ([]ListTapinOnly, error) {
	db := app.Appl.PostgreDB

	var output []ListTapinOnly

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.sp_vc_get_list_tapinonly($1,$2,$3);", periode, moMt, postPre)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ListTapinOnly{}
		rows.Scan(
			&scanProcess.FileID, &scanProcess.Periode, &scanProcess.BatchIDAsal, &scanProcess.FileTypeID,
			&scanProcess.PathFile, &scanProcess.Filename, &scanProcess.MoMt, &scanProcess.PostPre,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// GetLevelVolcomp : Function to Get Level of Volcomp
func (p *ReconTapinOnlyModel) GetLevelVolcomp() ([]LevelVolcomp, error) {
	db := app.Appl.PostgreDB

	var levelVolcomp []LevelVolcomp

	rows, err := db.Query("select level from roaming_vc_schema.vc_t_level_volcomp GROUP BY level order by level;")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	for rows.Next() {
		scanProcess := LevelVolcomp{}

		rows.Scan(&scanProcess.Level)

		levelVolcompConf := map[string]LevelVolcompConf{}
		rowsConf, err := db.Query("select * from roaming_vc_schema.vc_t_level_volcomp WHERE level = $1 ORDER BY id_level ASC;", scanProcess.Level)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		for rowsConf.Next() {
			scanProcess := LevelVolcompConf{}
			var scanParameterCompare string
			var arParameterCompare []string

			var scanParameterToleransi string
			var arParameterToleransi []string
			rowsConf.Scan(&scanProcess.IDLevel, &scanProcess.Level, &scanProcess.Type, &scanParameterCompare, &scanParameterToleransi)

			columns := strings.Split(scanParameterCompare, "|")
			for _, v := range columns {
				var urutan string
				err := db.QueryRow("SELECT urutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 AND fieldname = $2", scanProcess.Type, v).Scan(&urutan)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}
				arParameterCompare = append(arParameterCompare, urutan)
			}

			columns = strings.Split(scanParameterToleransi, "|")
			for _, v := range columns {
				var urutan string
				err := db.QueryRow("SELECT urutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 AND fieldname = $2", scanProcess.Type, v).Scan(&urutan)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}
				arParameterToleransi = append(arParameterToleransi, urutan)
			}

			scanProcess.ParameterCompare = arParameterCompare
			scanProcess.ParameterToleransi = arParameterToleransi

			levelVolcompConf[scanProcess.Type] = scanProcess
		}

		toleransiVolcomp := []ToleransiVolcomp{}
		// rowsToleransi, err := db.Query("select * from roaming_vc_schema.vc_t_conf_volcomp WHERE level = $1 ORDER BY priorities ASC;", scanProcess.Level)
		rowsToleransi, err := db.Query("select a.* from roaming_vc_schema.vc_t_conf_volcomp a "+
			"left join roaming_vc_schema.vc_p_sysconf b on b.sysconf = 'TOLERANSI_TAPIN_ONLY_ID' "+
			"WHERE level = $1 and a.active_id = b.valuesysconf::int "+
			"ORDER BY priorities ASC;", scanProcess.Level)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		for rowsToleransi.Next() {
			scanToleransi := ToleransiVolcomp{}
			rowsToleransi.Scan(&scanToleransi.IDToleransi, &scanToleransi.Level, &scanToleransi.Startcall,
				&scanToleransi.Durasi, &scanToleransi.Priorities, &scanToleransi.ActiveID)
			toleransiVolcomp = append(toleransiVolcomp, scanToleransi)
		}

		scanProcess.Toleransi = toleransiVolcomp
		scanProcess.Conf = levelVolcompConf
		levelVolcomp = append(levelVolcomp, scanProcess)
	}

	return levelVolcomp, nil
}
