package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// ConfigurationModel :
type ConfigurationModel struct{}

// GetDataStructure : Function to Get Data Structure from Postgresql
func (p *ConfigurationModel) GetDataStructure() (DataStructure, error) {
	db := app.Appl.PostgreDB

	var dataStruktur DataStructure

	dataStrukturParameter := map[string]DataStructureParameter{}

	// perform a db.Query
	rows, err := db.Query("SELECT data_type FROM roaming_vc_schema.vc_p_data_type Order By data_type_id")
	// if there is an error selecting, handle it
	if err != nil {
		return dataStruktur, err
	}

	for rows.Next() {
		var typeData string
		rows.Scan(&typeData)
		rowsParameter, err := db.Query("SELECT fieldname, urutan, sorturutan FROM roaming_vc_schema.vc_p_data_struktur WHERE type = $1 ORDER BY data_id", typeData)
		if err != nil {
			return dataStruktur, err
		}

		dtStParam := DataStructureParameter{}
		mapDataStructureProperties := map[string]DataStructureProperties{}
		for rowsParameter.Next() {
			scanProcess := DataStructureProperties{}
			var nameparam string
			rowsParameter.Scan(&nameparam, &scanProcess.Urutan, &scanProcess.SortUrutan)
			mapDataStructureProperties[nameparam] = scanProcess
		}
		dtStParam.Parameter = mapDataStructureProperties

		if len(mapDataStructureProperties) != 0 {
			dataStrukturParameter[typeData] = dtStParam
		}
	}

	dataStruktur.TypeData = dataStrukturParameter

	return dataStruktur, nil
}

// GetDataType : Function to Get Data Type from Postgresql
func (p *ConfigurationModel) GetDataType() ([]DataType, error) {
	db := app.Appl.PostgreDB

	var dataType []DataType

	// query of select
	query := "SELECT * FROM roaming_vc_schema.vc_p_data_type Order By data_type_id"

	// perform a db.Query
	rows, err := db.Query(query)
	// if there is an error selecting, handle it
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var DataTypeID int
		var DataTypeName string

		rows.Scan(&DataTypeID, &DataTypeName)

		dataType = append(dataType, DataType{DataTypeID, DataTypeName})
	}

	return dataType, nil
}

// GetSMTPConfig :
func (p *ConfigurationModel) GetSMTPConfig() (map[string]string, error) {
	db := app.Appl.PostgreDB

	output := make(map[string]string)

	rows, err := db.Query("select sysconf, valuesysconf from roaming_vc_schema.vc_p_sysconf where sysconf like 'smtp%';")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		var key string
		var value string
		rows.Scan(&key, &value)
		output[key] = value
	}

	return output, nil
}
