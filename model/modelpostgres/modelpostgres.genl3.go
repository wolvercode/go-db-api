package modelpostgres

import (
	"fmt"

	"bitbucket.org/billing/go-db-api/app"
)

// GenL3Model :
type GenL3Model struct{}

// GetProcessGenerateL3 : Function to Get Process GenerateL3
func (p *GenL3Model) GetProcessGenerateL3() ([]ProcessGenerateL3, error) {
	db := app.Appl.PostgreDB

	var outProcess []ProcessGenerateL3

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.v_get_process_l3;")
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := ProcessGenerateL3{}
		rows.Scan(
			&scanProcess.ProcessID, &scanProcess.BatchID, &scanProcess.Periode, &scanProcess.PostPre, &scanProcess.MoMt,
			&scanProcess.FileTypeMatch, &scanProcess.PathMatch, &scanProcess.FileMatch,
			&scanProcess.FileTypeOCSOnly, &scanProcess.PathOCSOnly, &scanProcess.FileOCSOnly,
			&scanProcess.FileTypeTAPINOnly, &scanProcess.PathTAPINOnly, &scanProcess.FileTAPINOnly,
			&scanProcess.FileTypeOCSDup, &scanProcess.PathOCSDup, &scanProcess.FileOCSDup,
			&scanProcess.FileTypeTAPINDup, &scanProcess.PathTAPINDup, &scanProcess.FileTAPINDup,
		)

		outProcess = append(outProcess, scanProcess)
	}

	return outProcess, nil
}

// GetDataL3 : Function to Get Data L3 Only & Dup
func (p *GenL3Model) GetDataL3(periode, postPre, moMt, dataType string) ([]StructDataL3, error) {
	db := app.Appl.PostgreDB

	var output []StructDataL3

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.sp_vc_get_data_l3($1, $2, $3, $4);", periode, postPre, moMt, dataType)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructDataL3{}
		rows.Scan(
			&scanProcess.PartnerID,
			&scanProcess.StartTime,
			&scanProcess.Anum,
			&scanProcess.Bnum,
			&scanProcess.Imsi,
			&scanProcess.Duration,
			&scanProcess.Charge,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// GetDataL3Match : Function to Get Data L3 Match
func (p *GenL3Model) GetDataL3Match(periode, postPre, moMt string) ([]StructDataL3Match, error) {
	db := app.Appl.PostgreDB

	var output []StructDataL3Match

	// perform a db.Query
	rows, err := db.Query("SELECT * FROM roaming_vc_schema.sp_vc_get_data_l3_match($1, $2, $3);", periode, postPre, moMt)
	// if there is an error selecting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		scanProcess := StructDataL3Match{}
		rows.Scan(
			&scanProcess.PartnerID,
			&scanProcess.StartTimeTapin,
			&scanProcess.AnumTapin,
			&scanProcess.BnumTapin,
			&scanProcess.ImsiTapin,
			&scanProcess.DurationTapin,
			&scanProcess.ChargeTapin,
			&scanProcess.StartTimeOCS,
			&scanProcess.AnumOCS,
			&scanProcess.BnumOCS,
			&scanProcess.ImsiOCS,
			&scanProcess.DurationOCS,
			&scanProcess.ChargeOCS,
			&scanProcess.TimeDiff,
			&scanProcess.DurationDiff,
			&scanProcess.ChargeDiff,
		)

		output = append(output, scanProcess)
	}

	return output, nil
}

// CloseProcessGenerateL3 : Function to Close Process Generate l3
func (p *GenL3Model) CloseProcessGenerateL3(param StructCloseGenerateL3) error {
	db := app.Appl.PostgreDB
	// perform a db.Query insert
	statement, err := db.Query("SELECT roaming_vc_schema.sp_vc_close_genl3($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);",
		param.ProcessConf.ProcessID, param.ProcessConf.BatchID, param.ProcessConf.Periode, param.ProcessStatusID, param.ErrorMessage,
		param.ProcessConf.FileTypeMatch, param.ProcessConf.PathMatch, param.ProcessConf.FileMatch,
		param.ProcessConf.FileTypeOCSOnly, param.ProcessConf.PathOCSOnly, param.ProcessConf.FileOCSOnly,
		param.ProcessConf.FileTypeTAPINOnly, param.ProcessConf.PathTAPINOnly, param.ProcessConf.FileTAPINOnly,
		param.ProcessConf.FileTypeOCSDup, param.ProcessConf.PathOCSDup, param.ProcessConf.FileOCSDup,
		param.ProcessConf.FileTypeTAPINDup, param.ProcessConf.PathTAPINDup, param.ProcessConf.FileTAPINDup,
	)
	// if there is an error inserting, handle it
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// be careful deferring Queries if you are using transactions
	defer statement.Close()

	return nil
}
