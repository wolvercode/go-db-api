package handlerpostgres

import (
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// ReconTapinOnlyHandler :
type ReconTapinOnlyHandler struct{}

// ModelReconTapinOnly : Model used by this handler
var ModelReconTapinOnly modelpostgres.ReconTapinOnlyModel

// GetProcessReconTapinOnly :
func (a *ReconTapinOnlyHandler) GetProcessReconTapinOnly(w http.ResponseWriter, r *http.Request) {
	processRekon, err := ModelReconTapinOnly.GetProcessReconTapinOnly()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, processRekon)
}

// GetListTapinOnly :
func (a *ReconTapinOnlyHandler) GetListTapinOnly(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	output, err := ModelReconTapinOnly.GetListTapinOnly(vars["periode"], vars["moMt"], vars["postPre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetLevelVolcomp :
func (a *ReconTapinOnlyHandler) GetLevelVolcomp(w http.ResponseWriter, r *http.Request) {
	output, err := ModelReconTapinOnly.GetLevelVolcomp()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}
