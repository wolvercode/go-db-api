package handlerpostgres

import (
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// TapCodeHandler :
type TapCodeHandler struct{}

// ModelTapCodeHandler :
var ModelTapCodeHandler modelpostgres.TapCodeModel

// GetTapCode :
func (a *TapCodeHandler) GetTapCode(w http.ResponseWriter, r *http.Request) {
	output, err := ModelTapCodeHandler.GetTapCode()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}
