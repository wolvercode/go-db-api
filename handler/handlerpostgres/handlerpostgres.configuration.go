package handlerpostgres

import (
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// PostgreSQLConfiguration :
type PostgreSQLConfiguration struct{}

var modelConfiguration modelpostgres.ConfigurationModel

// GetDataStructure :
func (a *PostgreSQLConfiguration) GetDataStructure(w http.ResponseWriter, r *http.Request) {

	dataStructure, err := modelConfiguration.GetDataStructure()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, dataStructure)
}

// GetDataType :
func (a *PostgreSQLConfiguration) GetDataType(w http.ResponseWriter, r *http.Request) {

	dataTypeList, err := modelConfiguration.GetDataType()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, dataTypeList)
}

// GetSMTPConfig :
func (a *PostgreSQLConfiguration) GetSMTPConfig(w http.ResponseWriter, r *http.Request) {
	output, err := modelConfiguration.GetSMTPConfig()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}
