package handlerpostgres

import (
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// ProcessHandler :
type ProcessHandler struct{}

// ModelPostgreSQLProcess :
var ModelPostgreSQLProcess modelpostgres.ProcessModel

// UpdateStatusProcess :
func (a *ProcessHandler) UpdateStatusProcess(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	if err := ModelPostgreSQLProcess.UpdateStatusProcess(vars["processID"], vars["processStatusID"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetSlicedProcess :
func (a *ProcessHandler) GetSlicedProcess(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	output, err := ModelPostgreSQLProcess.GetSlicedProcess(vars["minStartcall"], vars["maxStartcall"], vars["dataTypeID"], vars["moMt"], vars["postPre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}
