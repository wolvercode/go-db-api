package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// ReportHandler :
type ReportHandler struct{}

// ModelReport : Model used by this handler
var ModelReport modelpostgres.ReportModel

// GetProcessReport :
func (a *ReportHandler) GetProcessReport(w http.ResponseWriter, r *http.Request) {
	outProcess, err := ModelReport.GetProcessReport()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogMatch :
func (a *ReportHandler) GetLogMatch(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogMatch(vars["batchID"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogTapinOnly :
func (a *ReportHandler) GetLogTapinOnly(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogTapinOnly(vars["batchID"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// CloseProcessReport :
func (a *ReportHandler) CloseProcessReport(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseReport

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelReport.CloseProcessReport(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetProcessReportMonthly :
func (a *ReportHandler) GetProcessReportMonthly(w http.ResponseWriter, r *http.Request) {
	outProcess, err := ModelReport.GetProcessReportMonthly()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogReconMonthly :
func (a *ReportHandler) GetLogReconMonthly(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogReconMonthly(vars["periode"], vars["momt"], vars["postpre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogCheckDup :
func (a *ReportHandler) GetLogCheckDup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogCheckDup(vars["periode"], vars["momt"], vars["postpre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogOCSOnly :
func (a *ReportHandler) GetLogOCSOnly(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogOCSOnly(vars["periode"], vars["momt"], vars["postpre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogRecon :
func (a *ReportHandler) GetLogRecon(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogRecon(vars["periode"], vars["momt"], vars["postpre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogMatchToleransiGroup :
func (a *ReportHandler) GetLogMatchToleransiGroup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogMatchToleransiGroup(vars["periode"], vars["momt"], vars["postpre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogReportMonthly :
func (a *ReportHandler) GetLogReportMonthly(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogReportMonthly(vars["periode"], vars["momt"], vars["postpre"], vars["datatypeid"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogCheckDupPartnerID :
func (a *ReportHandler) GetLogCheckDupPartnerID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogCheckDupPartnerID(vars["periode"], vars["momt"], vars["postpre"], vars["datatype"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetLogCheckDupTopTen :
func (a *ReportHandler) GetLogCheckDupTopTen(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := ModelReport.GetLogCheckDupTopTen(vars["periode"], vars["momt"], vars["postpre"], vars["datatype"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// CloseProcessReportMonthly :
func (a *ReportHandler) CloseProcessReportMonthly(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseReportMonthly

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelReport.CloseProcessReportMonthly(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
