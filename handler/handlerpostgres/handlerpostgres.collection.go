package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// CollectionHandler :
type CollectionHandler struct{}

// ModelCollection : Model used by this handler
var ModelCollection modelpostgres.CollectionModel

// GetSSHost :
func (a *CollectionHandler) GetSSHost(w http.ResponseWriter, r *http.Request) {
	output, err := ModelCollection.GetSSHost()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetSSUser :
func (a *CollectionHandler) GetSSUser(w http.ResponseWriter, r *http.Request) {
	output, err := ModelCollection.GetSSUser()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetSSPass :
func (a *CollectionHandler) GetSSPass(w http.ResponseWriter, r *http.Request) {
	output, err := ModelCollection.GetSSPass()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetSSHome :
func (a *CollectionHandler) GetSSHome(w http.ResponseWriter, r *http.Request) {
	output, err := ModelCollection.GetSSHome()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetPathByFiletype :
func (a *CollectionHandler) GetPathByFiletype(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	output, err := ModelCollection.GetPathByFiletype(vars["filetypeID"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetSSTarget :
func (a *CollectionHandler) GetSSTarget(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	output, err := ModelCollection.GetSSTarget(vars["filetypeName"], vars["datatypeID"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// CloseProcessCollection :
func (a *CollectionHandler) CloseProcessCollection(w http.ResponseWriter, r *http.Request) {
	var modelCloseCollection modelpostgres.StructCloseCollection

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelCloseCollection); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelCollection.CloseProcessCollection(modelCloseCollection); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
