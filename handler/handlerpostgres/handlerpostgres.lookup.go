package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// LookupHandler :
type LookupHandler struct{}

// ModelLookup : Model used by this handler
var ModelLookup modelpostgres.LookupModel

// GetProcessSplit :
func (a *LookupHandler) GetProcessSplit(w http.ResponseWriter, r *http.Request) {
	output, err := ModelLookup.GetProcessSplit()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetListPartnerID :
func (a *LookupHandler) GetListPartnerID(w http.ResponseWriter, r *http.Request) {
	output, err := ModelLookup.GetListPartnerID()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// CloseProcessSplit :
func (a *LookupHandler) CloseProcessSplit(w http.ResponseWriter, r *http.Request) {
	var inputModel modelpostgres.ClosingSplit

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&inputModel); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelLookup.CloseProcessSplit(inputModel); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetProcessLookup :
func (a *LookupHandler) GetProcessLookup(w http.ResponseWriter, r *http.Request) {
	processLookup, err := ModelLookup.GetProcessLookup()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, processLookup)
}

// CloseProcessLookup :
func (a *LookupHandler) CloseProcessLookup(w http.ResponseWriter, r *http.Request) {
	var modelLookup modelpostgres.StructCloseLookup

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelLookup); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelLookup.CloseProcessLookup(modelLookup); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetLastLookup :
func (a *LookupHandler) GetLastLookup(w http.ResponseWriter, r *http.Request) {
	output, err := ModelLookup.GetLastLookup()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// UpdateLookup :
func (a *LookupHandler) UpdateLookup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	if err := ModelPostgreSQLProcess.UpdateLookup(vars["currentLookup"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
