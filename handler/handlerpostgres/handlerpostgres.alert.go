package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// AlertHandler :
type AlertHandler struct{}

// ModelAlert : Model used by this handler
var ModelAlert modelpostgres.AlertModel

// GetProcessAlert :
func (a *AlertHandler) GetProcessAlert(w http.ResponseWriter, r *http.Request) {
	outProcess, err := ModelAlert.GetProcessAlert()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// UpdateLastAlert :
func (a *AlertHandler) UpdateLastAlert(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	if err := ModelAlert.UpdateLastAlert(vars["datetime"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// MakeMailTask :
func (a *AlertHandler) MakeMailTask(w http.ResponseWriter, r *http.Request) {
	var structInput modelpostgres.MailTask

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&structInput); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelAlert.MakeMailTask(structInput); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetMailTask :
func (a *AlertHandler) GetMailTask(w http.ResponseWriter, r *http.Request) {
	outProcess, err := ModelAlert.GetMailTask()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// UpdateTaskStatus :
func (a *AlertHandler) UpdateTaskStatus(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	if err := ModelAlert.UpdateTaskStatus(vars["taskID"], vars["taskStatus"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
