package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// InsertDBHandler :
type InsertDBHandler struct{}

// ModelInsertDB : Model used by this handler
var ModelInsertDB modelpostgres.InsertDBModel

// GetProcessInsertDB :
func (a *InsertDBHandler) GetProcessInsertDB(w http.ResponseWriter, r *http.Request) {
	output, err := ModelInsertDB.GetProcessInsertDB()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// CreateTableIfNotExists :
func (a *InsertDBHandler) CreateTableIfNotExists(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	err := ModelInsertDB.CreateTableIfNotExists(vars["periode"], vars["dataType"], vars["fileType"], vars["postPre"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "Success"})
}

// InsertOCSToDB :
func (a *InsertDBHandler) InsertOCSToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var modelData modelpostgres.DataOCS

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelData); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.InsertOCSToDB(&modelData, vars["periode"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// InsertTAPINToDB :
func (a *InsertDBHandler) InsertTAPINToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var modelData modelpostgres.DataTAPIN

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelData); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.InsertTAPINToDB(&modelData, vars["periode"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// InsertTAPINOnlyToDB :
func (a *InsertDBHandler) InsertTAPINOnlyToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var modelData modelpostgres.DataTAPINOnly

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelData); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.InsertTAPINOnlyToDB(&modelData, vars["periode"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// CopyOCSToDB :
func (a *InsertDBHandler) CopyOCSToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var inputModel modelpostgres.StructCopyToTable

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&inputModel); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.CopyOCSToDB(&inputModel, vars["postPre"], vars["fileType"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// CopyTapinToDB :
func (a *InsertDBHandler) CopyTapinToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var inputModel modelpostgres.StructCopyToTable

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&inputModel); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.CopyTapinToDB(&inputModel, vars["postPre"], vars["fileType"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// CopyTapinOnlyToDB :
func (a *InsertDBHandler) CopyTapinOnlyToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var inputModel modelpostgres.StructCopyToTable

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&inputModel); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.CopyTapinOnlyToDB(&inputModel, vars["postPre"], vars["fileType"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// CopyMatchToDB :
func (a *InsertDBHandler) CopyMatchToDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var inputModel modelpostgres.StructCopyToTable

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&inputModel); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.CopyMatchToDB(&inputModel, vars["postPre"], vars["fileType"]); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// CloseProcessInsertDB :
func (a *InsertDBHandler) CloseProcessInsertDB(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseInsertDB

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.CloseProcessInsertDB(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetTaskInsertDB :
func (a *InsertDBHandler) GetTaskInsertDB(w http.ResponseWriter, r *http.Request) {
	output, err := ModelInsertDB.GetTaskInsertDB()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// UpdateStatusProcess :
func (a *InsertDBHandler) UpdateStatusProcess(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var errMsg string

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&errMsg); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelInsertDB.UpdateStatusProcess(vars["processID"], vars["processStatusID"], errMsg); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
