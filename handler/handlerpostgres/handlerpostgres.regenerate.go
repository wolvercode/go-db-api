package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// RegenerateHandler :
type RegenerateHandler struct{}

// ModelRegenerate : Model used by this handler
var ModelRegenerate modelpostgres.RegenerateModel

// GetRegenerateProcess :
func (a *RegenerateHandler) GetRegenerateProcess(w http.ResponseWriter, r *http.Request) {
	output, err := ModelRegenerate.GetRegenerateProcess()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// GetRegenerateData :
func (a *RegenerateHandler) GetRegenerateData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	output, err := ModelRegenerate.GetRegenerateData(vars["tablename"], vars["batchid"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// CloseRegenerateProcess :
func (a *RegenerateHandler) CloseRegenerateProcess(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseRegenerate

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelRegenerate.CloseRegenerateProcess(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
