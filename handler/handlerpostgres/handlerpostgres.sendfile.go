package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// SendFileHandler :
type SendFileHandler struct{}

// ModelSendFile : Model used by this handler
var ModelSendFile modelpostgres.SendFileModel

// GetProcessSendFile :
func (a *SendFileHandler) GetProcessSendFile(w http.ResponseWriter, r *http.Request) {
	outProcess, err := ModelSendFile.GetProcessSendFile()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetReceiverSendFile :
func (a *SendFileHandler) GetReceiverSendFile(w http.ResponseWriter, r *http.Request) {
	output, err := ModelSendFile.GetReceiverSendFile()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// CloseProcessSendFile :
func (a *SendFileHandler) CloseProcessSendFile(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseSendFile

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelSendFile.CloseProcessSendFile(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
