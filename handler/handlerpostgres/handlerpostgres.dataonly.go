package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// DataOnlyHandler :
type DataOnlyHandler struct{}

// ModelDataOnly : Model used by this handler
var ModelDataOnly modelpostgres.DataOnlyModel

// GetDataOnlyProcess :
func (a *DataOnlyHandler) GetDataOnlyProcess(w http.ResponseWriter, r *http.Request) {
	output, err := ModelDataOnly.GetDataOnlyProcess()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}

// CloseProcessDataOnly :
func (a *DataOnlyHandler) CloseProcessDataOnly(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseDataOnly

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelDataOnly.CloseProcessDataOnly(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
