package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// ReconHandler :
type ReconHandler struct{}

// ModelRecon : Model used by this handler
var ModelRecon modelpostgres.ReconModel

// GetProcessRecon :
func (a *ReconHandler) GetProcessRecon(w http.ResponseWriter, r *http.Request) {
	processRekon, err := ModelRecon.GetProcessRecon()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, processRekon)
}

// CloseProcessRecon :
func (a *ReconHandler) CloseProcessRecon(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseRecon

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelRecon.CloseProcessRecon(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetLevelVolcomp :
func (a *ReconHandler) GetLevelVolcomp(w http.ResponseWriter, r *http.Request) {
	output, err := ModelRecon.GetLevelVolcomp()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}
