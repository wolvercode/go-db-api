package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
	"github.com/gorilla/mux"
)

// GenL3Handler :
type GenL3Handler struct{}

// usedModel : Model used by this handler
var usedModel modelpostgres.GenL3Model

// GetProcessGenerateL3 :
func (a *GenL3Handler) GetProcessGenerateL3(w http.ResponseWriter, r *http.Request) {
	outProcess, err := usedModel.GetProcessGenerateL3()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetDataL3 :
func (a *GenL3Handler) GetDataL3(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := usedModel.GetDataL3(vars["periode"], vars["postpre"], vars["momt"], vars["datatype"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// GetDataL3Match :
func (a *GenL3Handler) GetDataL3Match(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	outProcess, err := usedModel.GetDataL3Match(vars["periode"], vars["postpre"], vars["momt"])
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, outProcess)
}

// CloseProcessGenerateL3 :
func (a *GenL3Handler) CloseProcessGenerateL3(w http.ResponseWriter, r *http.Request) {
	var modelClose modelpostgres.StructCloseGenerateL3

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelClose); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := usedModel.CloseProcessGenerateL3(modelClose); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}
