package handlerpostgres

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/billing/go-db-api/common"
	"bitbucket.org/billing/go-db-api/model/modelpostgres"
)

// CheckdupHandler :
type CheckdupHandler struct{}

// ModelCheckDup : Model used by this handler
var ModelCheckDup modelpostgres.CheckDupModel

// GetProcessCheckDup :
func (a *CheckdupHandler) GetProcessCheckDup(w http.ResponseWriter, r *http.Request) {
	processCheckdup, err := ModelCheckDup.GetProcessCheckDup()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, processCheckdup)
}

// CloseProcessCheckdup :
func (a *CheckdupHandler) CloseProcessCheckdup(w http.ResponseWriter, r *http.Request) {
	var modelCloseCheckDup modelpostgres.StructCloseCheckDup

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&modelCloseCheckDup); err != nil {
		common.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := ModelCheckDup.CloseProcessCheckDup(modelCloseCheckDup); err != nil {
		common.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Success"})
}

// GetCheckDupConf :
func (a *CheckdupHandler) GetCheckDupConf(w http.ResponseWriter, r *http.Request) {
	output, err := ModelCheckDup.GetCheckDupConf()
	if err != nil {
		common.RespondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	common.RespondWithJSON(w, http.StatusOK, output)
}
