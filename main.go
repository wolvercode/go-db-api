package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/billing/go-db-api/app"
	"bitbucket.org/billing/go-db-api/routes"
)

func main() {
	setupCloseHandler()
	app.Initialize()

	routes.InitializeRoutes(app.Appl.Configs.Main.Port)
}

func setupCloseHandler() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		app.Appl.PostgreDB.Close()
		os.Exit(0)
	}()
}
